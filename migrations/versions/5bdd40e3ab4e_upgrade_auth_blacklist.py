"""Upgrade Auth Blacklist

Revision ID: 5bdd40e3ab4e
Revises: 5deb4103f8c7
Create Date: 2023-09-06 22:58:56.464177

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "5bdd40e3ab4e"
down_revision: Union[str, None] = "5deb4103f8c7"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # Add `id` column and set it as primary key
    op.add_column(
        "auth",
        sa.Column("id", sa.Integer, primary_key=True, nullable=False),
        schema="namdo",
    )

    # Step 1: Add `uid` column without NOT NULL constraint
    op.add_column("auth", sa.Column("uid", sa.String(40)), schema="namdo")

    # Step 2: Populate `uid` column (this is just a placeholder logic, adjust as needed)
    op.execute(
        "UPDATE namdo.auth SET uid = did::VARCHAR(40)"
    )  # Here, I'm copying from `did` as an example.

    # Step 3: Alter `uid` column to add NOT NULL constraint
    op.alter_column("auth", "uid", nullable=False, schema="namdo")

    # Add `expired` column without NOT NULL constraint
    op.add_column("auth", sa.Column("expired", sa.Boolean), schema="namdo")

    # Set a default value for the new `expired` column for all rows
    op.execute("UPDATE namdo.auth SET expired = False")

    # Alter the `expired` column to enforce the NOT NULL constraint
    op.alter_column("auth", "expired", nullable=False, schema="namdo")

    # Remove primary key constraint from `did` column (this is a little complex in Alembic)
    op.drop_constraint("auth_pkey", "auth", type_="primary", schema="namdo")
    op.create_primary_key(None, "auth", ["id"], schema="namdo")


def downgrade() -> None:
    # Remove `expired` column
    op.drop_column("auth", "expired", schema="namdo")

    # Remove `uid` column
    op.drop_column("auth", "uid", schema="namdo")

    # Remove `id` column
    op.drop_constraint("auth_pkey", "auth", type_="primary", schema="namdo")
    op.drop_column("auth", "id", schema="namdo")

    # Restore primary key constraint to `did` column
    op.create_primary_key("auth_pkey", "auth", ["did"], schema="namdo")
