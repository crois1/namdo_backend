from enum import Enum
from datetime import datetime
from zoneinfo import ZoneInfo
from pydantic import BaseModel, Field, BeforeValidator
from typing_extensions import Annotated


def parse_date_string(value: str) -> datetime:
    try:
        # Convert the input date string to a timezone-aware datetime
        return datetime.strptime(value, "%Y%m%d").replace(
            tzinfo=ZoneInfo("Asia/Seoul")
        )
    except Exception as e:
        raise ValueError(f"{e}: {value}. Use the format '%Y%m%d'.")


def add_timezone(value: datetime) -> datetime:
    try:
        # Convert the input datetime to a timezone-aware datetime
        return value.astimezone(ZoneInfo("Asia/Seoul"))
    except Exception as e:
        raise ValueError(f"{e}: {value}.")


class PlanState(str, Enum):
    undone = "Undone"
    editting = "Editting"
    working = "Working"
    done = "Done"


class BOMState(str, Enum):
    undone = "Undone"
    editting = "Editting"
    done = "Done"


class Plan_Request(BaseModel):
    company: str = Field(min_length=1)
    lot: str | None = Field(default=None)
    material_unit: str | None = Field(default=None)
    material_amount: str | None = Field(default=None)
    product_name: str = Field(min_length=1)
    product_unit: str = Field(min_length=1)
    amount: int
    deadline: str | None = Field(default=None)
    note: str | None = Field(default=None)


class Plan_Create_Request(Plan_Request):
    madedate: Annotated[datetime, BeforeValidator(parse_date_string)]


class Plan_Update_Request(Plan_Request):
    id: int


class Plan_UpdateState_Request(BaseModel):
    id: int


class PlanDate_Parameters(BaseModel):
    pass


class PlanAdmin_Read_Parameters(PlanDate_Parameters):
    # madedate: str
    madedate: Annotated[datetime, BeforeValidator(parse_date_string)]


class PlanDetail_Read_Parameters(PlanDate_Parameters):
    # start_date: str
    # end_date: str
    start_date: Annotated[datetime, BeforeValidator(parse_date_string)]
    end_date: Annotated[datetime, BeforeValidator(parse_date_string)]


class Plan_Response(BaseModel):
    id: int
    state: PlanState
    madedate: Annotated[datetime, BeforeValidator(add_timezone)]
    company: str
    lot: str | None = Field(default=None)
    material_unit: str | None = Field(default=None)
    material_amount: str | None = Field(default=None)
    product_name: str
    product_unit: str
    amount: int
    deadline: str | None = Field(default=None)
    note: str | None = Field(default=None)
    bom_state: BOMState
    background_color: str


class Plan_Create_Response(Plan_Response):
    pass


class Plan_Update_Response(Plan_Response):
    pass


class Plan_UpdateState_Response(Plan_Response):
    pass


class Plan_Delete_Response(Plan_Response):
    pass


class PlanAdmin_Read_Response(BaseModel):
    id: int
    company: str
    lot: str | None = Field(default=None)
    material_unit: str | None = Field(default=None)
    material_amount: str | None = Field(default=None)
    product_name: str
    product_unit: str
    amount: int
    deadline: str | None = Field(default=None)
    note: str | None = Field(default=None)


class PlanDetail_Read_Response(BaseModel):
    id: int
    state: PlanState
    company: str
    lot: str | None = Field(default=None)
    product_name: str
    product_unit: str
    amount: int
    deadline: str | None = Field(default=None)
    bom_state: BOMState
    background_color: str


class Plan_ReadState_Response(BaseModel):
    id: int
    state: PlanState
    bom_state: BOMState


class Plan_ReadGanttDate_Response(BaseModel):
    date_to_move: Annotated[datetime, BeforeValidator(add_timezone)]


class Plan_Calendar_Response(BaseModel):
    date: list[Annotated[datetime, BeforeValidator(add_timezone)]]
