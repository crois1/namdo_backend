from enum import Enum
from datetime import datetime
from pydantic import BaseModel, EmailStr, Field


class Role(str, Enum):
    worker = "Worker"
    admin = "Admin"
    master = "Master"


class User_Request(BaseModel):
    user_id: str = Field(min_length=1)
    pass_word: str = Field(min_length=1)
    name: str = Field(min_length=1)
    email: EmailStr
    role: Role


class User_Create_Request(User_Request):
    pass


class User_Update_Request(User_Request):
    id: int


class User_Response(BaseModel):
    id: int
    user_id: str
    name: str
    email: EmailStr
    role: Role


class User_Create_Response(User_Response):
    pass


class User_Update_Response(User_Response):
    pass


class User_Delete_Response(User_Response):
    deletedAt: datetime


class User_ReadAll_Response(User_Response):
    pass


class User_ReadAllNames_Response(BaseModel):
    name: str
