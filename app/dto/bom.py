from enum import Enum
from typing import List
from pydantic import BaseModel, Field


class BOMState(str, Enum):
    undone = "Undone"
    editting = "Editting"
    done = "Done"


class BOM_Request(BaseModel):
    bom_state: BOMState
    plan_id: int


class BOM_Create_Request(BOM_Request):
    process_name: str = Field(min_length=1)


class BOM_Update_Request(BOM_Request):
    process_list: List[int]


class BOM_Response(BaseModel):
    id: int
    plan_id: int
    process_name: str
    process_order: int


class BOM_Create_Response(BOM_Response):
    pass


class BOM_Read_Response(BOM_Response):
    pass


class BOM_Update_Response(BOM_Response):
    pass


class BOM_Delete_Response(BOM_Response):
    pass


class BOM_DeleteAll_Response(BOM_Response):
    pass
