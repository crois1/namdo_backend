from pydantic import BaseModel, Field


class Process_Request(BaseModel):
    process_name: str = Field(min_length=1)


class Process_Create_Request(Process_Request):
    pass


class Process_Update_Request(BaseModel):
    old_process_name: str = Field(min_length=1)
    new_process_name: str = Field(min_length=1)


class Process_Response(BaseModel):
    id: int
    process_name: str


class Process_Create_Response(Process_Response):
    pass


class Process_Read_Response(Process_Response):
    pass


class Process_Update_Response(Process_Response):
    pass


class Process_Delete_Response(Process_Response):
    pass
