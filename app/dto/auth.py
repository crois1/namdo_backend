from enum import Enum
from pydantic import BaseModel, Field, EmailStr


class Role(str, Enum):
    worker = "Worker"
    admin = "Admin"
    master = "Master"


class Auth_Request(BaseModel):
    user_id: str = Field(min_length=1)
    pass_word: str = Field(min_length=1)
    name: str = Field(min_length=1)
    email: EmailStr


class AuthSignUp_Request(Auth_Request):
    pass


class Auth_Response(BaseModel):
    user_id: str
    name: str
    role: Role


class AuthLogin_Response(Auth_Response):
    access_token: str
    token_type: str
    expires_in: int
    # refresh_token: str


class AuthSignUp_Response(Auth_Response):
    id: int
    email: EmailStr
