from datetime import datetime
from zoneinfo import ZoneInfo
from pydantic import BaseModel, Field, BeforeValidator
from typing_extensions import Annotated


def parse_date_string(value: str | None) -> datetime:
    if value is None:
        return None
    try:
        # Convert the input date string to a timezone-aware datetime
        return datetime.strptime(value, "%Y%m%d").replace(
            tzinfo=ZoneInfo("Asia/Seoul")
        )
    except Exception as e:
        raise ValueError(f"{e}: {value}. Use the format '%Y%m%d'.")


def add_timezone(value: datetime) -> datetime:
    try:
        # Convert the input datetime to a timezone-aware datetime
        return value.astimezone(ZoneInfo("Asia/Seoul"))
    except Exception as e:
        raise ValueError(f"{e}: {value}.")


class Dashboard_Parameters(BaseModel):
    start_date: Annotated[
        datetime | None, BeforeValidator(parse_date_string)
    ] | None = Field(default=None)
    end_date: Annotated[
        datetime | None, BeforeValidator(parse_date_string)
    ] | None = Field(default=None)


class DashboardMaster_Parameters(Dashboard_Parameters):
    name: str | None = Field(default="")


class Dashboard_ParametersTotal(Dashboard_Parameters):
    c: str
    pdn: str
    pdu: str
    prn: str
    fn: str


class Dashboard_Response(BaseModel):
    accomplishment: int
    workdate: Annotated[datetime, BeforeValidator(add_timezone)]
    facility_name: str
    process_name: str
    product_unit: str
    company: str
    lot: str
    product_name: str


class Dashboard_Master_Response(Dashboard_Response):
    id: int
    user_name: str
    note: str | None = Field(default=None)


class Dashboard_ResponseTotal(Dashboard_Response):
    pass
