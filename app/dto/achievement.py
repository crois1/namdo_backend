from datetime import datetime
from zoneinfo import ZoneInfo
from pydantic import BaseModel, Field, BeforeValidator
from typing_extensions import Annotated


def parse_date(value: str) -> datetime:
    try:
        return datetime.fromisoformat(value).replace(
            tzinfo=ZoneInfo("Asia/Seoul")
        )
    except Exception as e:
        raise ValueError(f"{e}: {value}.")


def add_timezone(value: datetime) -> datetime:
    try:
        # Convert the input datetime to a timezone-aware datetime
        return value.astimezone(ZoneInfo("Asia/Seoul"))
    except Exception as e:
        raise ValueError(f"{e}: {value}.")


class Achievement_Request(BaseModel):
    user_name: str = Field(min_length=1)
    gantt_id: int
    accomplishment: int
    note: str | None = Field(default=None)


class Achievement_Create_Request(Achievement_Request):
    pass


class Achievement_UpdateAccomplishment_Request(BaseModel):
    id: int
    accomplishment: int


class Achievement_UpdateWorkdate_Request(BaseModel):
    id: int
    workdate: Annotated[datetime, BeforeValidator(parse_date)]


class Achievement_UpdateNote_Request(BaseModel):
    id: int
    note: str | None = Field(default=None)


class Achievement_Response(BaseModel):
    id: int
    user_name: str
    workdate: Annotated[datetime, BeforeValidator(add_timezone)]
    accomplishment: int
    note: str | None


class Achievement_Create_Response(Achievement_Response):
    gantt_id: int


class AchievementDetail_Read_Response(Achievement_Response):
    gantt_id: int


class AchievementDetail_ReadAccomplishment_Response(BaseModel):
    left_accomplishment: int
    achievement_ratio: float


class AchievementMaster_Read_Response(Achievement_Response):
    facility_name: str
    process_name: str
    product_unit: str
    company: str
    product_name: str


class Achievement_UpdateAccomplishment_Response(Achievement_Response):
    gantt_id: int


class Achievement_UpdateWorkdate_Response(Achievement_Response):
    gantt_id: int


class Achievement_UpdateNote_Response(Achievement_Response):
    gantt_id: int


class Achievement_Delete_Response(Achievement_Response):
    gantt_id: int
