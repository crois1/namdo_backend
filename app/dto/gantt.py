from datetime import datetime
from zoneinfo import ZoneInfo
from pydantic import BaseModel, Field, BeforeValidator
from typing_extensions import Annotated


def parse_date(value: str) -> datetime:
    try:
        return datetime.fromisoformat(value).replace(
            tzinfo=ZoneInfo("Asia/Seoul")
        )
    except Exception as e:
        raise ValueError(f"{e}: {value}.")


def parse_date_string(value: str | None) -> datetime:
    if value is None:
        return None
    try:
        # Convert the input date string to a timezone-aware datetime
        return datetime.strptime(value, "%Y%m%d").replace(
            tzinfo=ZoneInfo("Asia/Seoul")
        )
    except Exception as e:
        raise ValueError(f"{e}: {value}. Use the format '%Y%m%d'.")


def add_timezone(value: datetime) -> datetime:
    try:
        # Convert the input datetime to a timezone-aware datetime
        return value.astimezone(ZoneInfo("Asia/Seoul"))
    except Exception as e:
        raise ValueError(f"{e}: {value}.")


class Gantt_Request(BaseModel):
    start_date: Annotated[datetime, BeforeValidator(parse_date)]
    end_date: Annotated[datetime, BeforeValidator(parse_date)]
    facility_name: str = Field(min_length=1)


class Gantt_Create_Request(Gantt_Request):
    bom_id: int


class Gantt_Update_Request(Gantt_Request):
    id: int


class Gantt_Parameters(BaseModel):
    pass


class Gantt_Read_Parameters(Gantt_Parameters):
    start_date: Annotated[datetime, BeforeValidator(parse_date_string)]
    end_date: Annotated[
        datetime | None, BeforeValidator(parse_date_string)
    ] = Field(default=None)


class Gantt_Response(BaseModel):
    id: int
    start_date: Annotated[datetime, BeforeValidator(add_timezone)]
    end_date: Annotated[datetime, BeforeValidator(add_timezone)]
    facility_name: str


class Gantt_Create_Response(Gantt_Response):
    bom_id: int


class Gantt_Read_Response(Gantt_Response):
    title: str
    background_color: str
    achievement_ratio: float


class Gantt_Update_Response(Gantt_Response):
    bom_id: int


class Gantt_Delete_Response(Gantt_Response):
    bom_id: int


class Gantt_Calendar_Response(BaseModel):
    date: list[Annotated[datetime, BeforeValidator(add_timezone)]]
