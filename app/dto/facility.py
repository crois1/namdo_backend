from pydantic import BaseModel, Field


class Facility_Request(BaseModel):
    facility_name: str = Field(min_length=1)


class Facility_Create_Request(Facility_Request):
    pass


class Facility_UpdateName_Request(BaseModel):
    old_facility_name: str = Field(min_length=1)
    new_facility_name: str = Field(min_length=1)


class Facility_UpdateOrder_Request(BaseModel):
    first_facility_name: str = Field(min_length=1)
    second_facility_name: str = Field(min_length=1)


class Facility_Response(BaseModel):
    id: int
    facility_name: str
    order: int


class Facility_Create_Response(Facility_Response):
    pass


class Facility_Read_Response(Facility_Response):
    pass


class Facility_UpdateName_Response(Facility_Response):
    pass


class Facility_UpdateOrder_Response(Facility_Response):
    pass


class Facility_Delete_Response(Facility_Response):
    pass
