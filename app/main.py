import os
import secrets
import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from app.db import connection
from app.routes import router

# from app.config import settings

# from app.libs.tokenUtil import middlewareToken
# secret_key = secrets.token_urlsafe(32)
# os.environ["SECRET_KEY"] = secret_key

# app initialization
app = FastAPI()

app.mount("/static", StaticFiles(directory="app/static"), name="static")

# CORS config
origins = [
    "*",
    #  "http://192.168.0.7",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"],
)

# mongodb - odmantic - connect
app.add_event_handler("startup", connection.on_app_start)
app.add_event_handler("shutdown", connection.on_app_shutdown)

# middleware
# app.middleware("http")(middlewareToken)

# routing
app.include_router(router)

# app runs
if __name__ == "__main__":
    uvicorn.run(
        "app.main:app",
        host="0.0.0.0",
        port=8000,
        # reload=True,
        reload=False,
        workers=4,
    )
