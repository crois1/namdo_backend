# import os
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base
from sqlalchemy.ext.asyncio import (
    create_async_engine,
    async_sessionmaker,
    AsyncSession,
)
from app.config import settings
from app.db.testdata import input_test_data

Base = declarative_base(metadata=MetaData(schema="namdo"))


class PostgreSQL:
    def __init__(self):
        self.engine = None
        self.session = None

    async def create(self):
        self.engine = create_async_engine(
            settings.POSTGRESQL_URL,
            pool_pre_ping=True,
            # echo=True,
        )
        self.session = async_sessionmaker(
            bind=self.engine,
            autoflush=False,
            autocommit=False,
            expire_on_commit=False,
        )

        async with self.engine.begin() as conn:
            # await conn.run_sync(Base.metadata.drop_all)
            await conn.run_sync(Base.metadata.create_all)

        # async with self.session() as session:
        #     await input_test_data(session)
        # print(settings.SECRET_KEY)
        print("db connected")

        # await self.session().close()

    async def connect(self) -> AsyncSession:
        async with self.session() as session:
            yield session

    async def close(self):
        print("db disconnected")
        await self.engine.dispose()


postgresql = PostgreSQL()
