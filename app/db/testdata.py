import random
from sqlalchemy import select, insert, func
from datetime import datetime
from app.libs.hashUtil import hash_password


def int_to_date(date_int):
    return datetime.strptime(str(date_int), "%Y%m%d")


def date_to_tz(dt):
    return datetime.strptime(str(dt), "%Y-%m-%dT%H:%M:%S%z")


def random_color():
    return "#{:02x}{:02x}{:02x}".format(
        random.randint(125, 255),
        random.randint(125, 255),
        random.randint(125, 255),
    )


async def input_test_data(session):
    # User Data
    from app.models.user import User

    try:
        query = select(func.count(User.id))
        result = await session.scalars(query)
        if result.one() < 1:
            await add_test_user_data(session)
    except Exception as e:
        print(f"Error: {e}")

    # Process Data
    from app.models.process import Process

    try:
        query = select(func.count(Process.id))
        result = await session.scalars(query)
        if result.one() < 1:
            await add_test_process_data(session)
    except Exception as e:
        print(f"Error: {e}")

    # Facility Data
    from app.models.facility import Facility

    try:
        query = select(func.count(Facility.id))
        result = await session.scalars(query)
        if result.one() < 1:
            await add_test_facility_data(session)
    except Exception as e:
        print(f"Error: {e}")

    # Plan Data
    # from app.models.plan import Plan

    # try:
    #     query = select(func.count(Plan.id))
    #     result = await session.scalars(query)
    #     if result.one() < 1:
    #         await add_test_plan_data(session)
    # except Exception as e:
    #     print(f"Error: {e}")

    # BOM Data
    # from app.models.bom import BOM

    # try:
    #     query = select(func.count(BOM.id))
    #     result = await session.scalars(query)
    #     if result.one() < 1:
    #         await add_test_bom_data(session)
    # except Exception as e:
    #     print(f"Error: {e}")

    # Gantt Data
    # from app.models.gantt import Gantt

    # try:
    #     query = select(func.count(Gantt.id))
    #     result = await session.scalars(query)
    #     if result.one() < 1:
    #         await add_test_gantt_data(session)
    # except Exception as e:
    #     print(f"Error: {e}")

    # Achievement Data
    # from app.models.achievement import Achievement

    # try:
    #     query = select(func.count(Achievement.id))
    #     result = await session.scalars(query)
    #     if result.one() < 1:
    #         await add_test_achievement_data(session)
    # except Exception as e:
    #     print(f"Error: {e}")

    # from app.services import plan_service
    # from app.dao import plan_dao

    # try:
    #     for i in range(1, 7):
    #         plan = await plan_dao.Read(i, session)
    #         await plan_service.set_plan_state(plan, session)

    # except Exception as e:
    #     print(f"Error: {e}")


async def add_test_user_data(session):
    from app.models.user import User

    try:
        query = insert(User).values(
            [
                {
                    "user_id": "Master",
                    "pass_word": hash_password("Master"),
                    "name": "마스터",
                    "email": "Master@master.com",
                    "role": "Master",
                },
                {
                    "user_id": "Admin",
                    "pass_word": hash_password("Admin"),
                    "name": "어드민",
                    "email": "Admin@admin.com",
                    "role": "Admin",
                },
                {
                    "user_id": "Worker",
                    "pass_word": hash_password("Worker"),
                    "name": "작업자",
                    "email": "Worker@worker.com",
                    "role": "Worker",
                },
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        print(f"Error: {e}")
        await session.rollback()


async def add_test_plan_data(session):
    from app.models.plan import Plan

    try:
        query = insert(Plan).values(
            [
                {
                    "madedate": int_to_date(20230901),
                    "company": "Alpha",
                    "product_name": "AAA",
                    "product_unit": "111",
                    "amount": 100,
                    "background_color": random_color(),
                    "bom_state": "Done",
                },
                {
                    "madedate": int_to_date(20230908),
                    "company": "Bravo",
                    "product_name": "BBB",
                    "product_unit": "111",
                    "amount": 200,
                    "background_color": random_color(),
                    "bom_state": "Done",
                },
                {
                    "madedate": int_to_date(20230915),
                    "company": "Charlie",
                    "product_name": "CCC",
                    "product_unit": "112",
                    "amount": 400,
                    "background_color": random_color(),
                    "bom_state": "Done",
                },
                {
                    "madedate": int_to_date(20230923),
                    "company": "Delta",
                    "product_name": "DDD",
                    "product_unit": "120",
                    "amount": 800,
                    "background_color": random_color(),
                    "bom_state": "Editting",
                },
                {
                    "madedate": int_to_date(20230930),
                    "company": "Echo",
                    "product_name": "EEE",
                    "product_unit": "120",
                    "amount": 1600,
                    "background_color": random_color(),
                },
                {
                    "madedate": int_to_date(20230801),
                    "company": "Foxtrot",
                    "product_name": "FFF",
                    "product_unit": "130",
                    "amount": 3200,
                    "background_color": random_color(),
                },
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        print(f"Error: {e}")
        await session.rollback()


async def add_test_process_data(session):
    from app.models.process import Process

    try:
        query = insert(Process).values(
            [
                {"process_name": "CNC 1차"},
                {"process_name": "CNC 2차"},
                {"process_name": "CNC 3차"},
                {"process_name": "CNC 4차"},
                {"process_name": "CNC 5차"},
                {"process_name": "CNC 6차"},
                {"process_name": "MCT 1차"},
                {"process_name": "MCT 2차"},
                {"process_name": "MCT 3차"},
                {"process_name": "MCT 4차"},
                {"process_name": "MCT 5차"},
                {"process_name": "MCT 6차"},
                {"process_name": "MCT 1+2+3차"},
                {"process_name": "내경나사"},
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        print(f"Error: {e}")
        await session.rollback()


async def add_test_facility_data(session):
    from app.models.facility import Facility

    try:
        query = insert(Facility).values(
            [
                {"facility_name": "ND1-01  QT30N", "order": 0},
                {"facility_name": "ND1-02  QT18N", "order": 1},
                {"facility_name": "ND1-04  QT28N", "order": 2},
                {"facility_name": "ND1-05  QT18N", "order": 3},
                {"facility_name": "ND1-06  QT18N", "order": 4},
                {"facility_name": "ND1-08  L210A", "order": 5},
                {"facility_name": "ND1-09  HD2200C", "order": 6},
                {"facility_name": "ND1-11  SKT-V5L", "order": 7},
                {"facility_name": "ND1-12  SKT-V5R", "order": 8},
                {"facility_name": "ND1-13  L300MA", "order": 9},
                {"facility_name": "ND1-14  L300A", "order": 10},
                {"facility_name": "ND1-15  PUMA GT2600XL", "order": 11},
                {"facility_name": "ND1-16  PUMA GT2600XL", "order": 12},
                {"facility_name": "ND1-17  PUMA 3100ULM", "order": 13},
                {"facility_name": "ND1-18  SHAFT 교정기", "order": 14},
                {"facility_name": "ND2-02  MYNX6500", "order": 15},
                {"facility_name": "ND2-03  VX650", "order": 16},
                {"facility_name": "ND2-04  VX500", "order": 17},
                {"facility_name": "ND2-05  F510B", "order": 18},
                {"facility_name": "ND2-06  F650", "order": 19},
                {"facility_name": "ND2-09  F650", "order": 20},
                {"facility_name": "ND2-08  MYNX5400", "order": 21},
                {"facility_name": "ND2-10  MYNX6500", "order": 22},
                {"facility_name": "ND2-11  VARI 730-5X 2", "order": 23},
                {"facility_name": "ND2-12  INTE-200-4T", "order": 24},
                {"facility_name": "ND3-01  S-21", "order": 25},
                {"facility_name": "ND3-02  S-21 THREAD", "order": 26},
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        print(f"Error: {e}")
        await session.rollback()


async def add_test_bom_data(session):
    from app.models.bom import BOM

    try:
        query = insert(BOM).values(
            [
                {
                    "plan_id": 1,
                    "process_name": "CNC 1차",
                    "process_order": 0,
                },
                {
                    "plan_id": 1,
                    "process_name": "CNC 2차",
                    "process_order": 1,
                },
                {
                    "plan_id": 1,
                    "process_name": "CNC 3차",
                    "process_order": 2,
                },
                {
                    "plan_id": 2,
                    "process_name": "CNC 1차",
                    "process_order": 0,
                },
                {
                    "plan_id": 2,
                    "process_name": "CNC 2차",
                    "process_order": 1,
                },
                {
                    "plan_id": 2,
                    "process_name": "CNC 3차",
                    "process_order": 2,
                },
                {
                    "plan_id": 2,
                    "process_name": "CNC 4차",
                    "process_order": 3,
                },
                {
                    "plan_id": 3,
                    "process_name": "MCT 1차",
                    "process_order": 0,
                },
                {
                    "plan_id": 3,
                    "process_name": "MCT 2차",
                    "process_order": 1,
                },
                {
                    "plan_id": 4,
                    "process_name": "MCT 1차",
                    "process_order": 0,
                },
                {
                    "plan_id": 4,
                    "process_name": "MCT 2차",
                    "process_order": 1,
                },
                {
                    "plan_id": 4,
                    "process_name": "MCT 3차",
                    "process_order": 2,
                },
                {
                    "plan_id": 4,
                    "process_name": "MCT 4차",
                    "process_order": 3,
                },
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        print(f"Error: {e}")
        await session.rollback()


async def add_test_gantt_data(session):
    from app.models.gantt import Gantt

    try:
        query = insert(Gantt).values(
            [
                {
                    "bom_id": 1,
                    "start_date": date_to_tz("2023-09-01T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-03T01:00:00+09:00"),
                    "facility_name": "ND2-02  MYNX6500",
                },
                {
                    "bom_id": 1,
                    "start_date": date_to_tz("2023-09-01T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-02T01:00:00+09:00"),
                    "facility_name": "ND2-03  VX650",
                },
                {
                    "bom_id": 2,
                    "start_date": date_to_tz("2023-09-04T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-06T01:00:00+09:00"),
                    "facility_name": "ND1-01  QT30N",
                },
                {
                    "bom_id": 3,
                    "start_date": date_to_tz("2023-09-08T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-10T01:00:00+09:00"),
                    "facility_name": "ND2-02  MYNX6500",
                },
                {
                    "bom_id": 4,
                    "start_date": date_to_tz("2023-09-08T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-09T01:00:00+09:00"),
                    "facility_name": "ND2-02  MYNX6500",
                },
                {
                    "bom_id": 4,
                    "start_date": date_to_tz("2023-09-08T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-10T01:00:00+09:00"),
                    "facility_name": "ND2-03  VX650",
                },
                {
                    "bom_id": 5,
                    "start_date": date_to_tz("2023-09-10T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-11T01:00:00+09:00"),
                    "facility_name": "ND1-01  QT30N",
                },
                {
                    "bom_id": 6,
                    "start_date": date_to_tz("2023-09-13T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-16T01:00:00+09:00"),
                    "facility_name": "ND2-02  MYNX6500",
                },
                {
                    "bom_id": 6,
                    "start_date": date_to_tz("2023-09-13T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-16T01:00:00+09:00"),
                    "facility_name": "ND2-03  VX650",
                },
                {
                    "bom_id": 7,
                    "start_date": date_to_tz("2023-09-17T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-18T01:00:00+09:00"),
                    "facility_name": "ND1-01  QT30N",
                },
                {
                    "bom_id": 7,
                    "start_date": date_to_tz("2023-09-17T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-18T01:00:00+09:00"),
                    "facility_name": "ND1-02  QT18N",
                },
                {
                    "bom_id": 8,
                    "start_date": date_to_tz("2023-09-17T01:00:00+09:00"),
                    "end_date": date_to_tz("2023-09-18T01:00:00+09:00"),
                    "facility_name": "ND2-03  VX650",
                },
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        print(f"Error: {e}")
        await session.rollback()


async def add_test_achievement_data(session):
    from app.models.achievement import Achievement

    try:
        query = insert(Achievement).values(
            [
                {
                    "gantt_id": 1,
                    "user_name": "어드민",
                    "accomplishment": 30,
                    "workdate": date_to_tz("2023-09-01T12:00:00+09:00"),
                },
                {
                    "gantt_id": 1,
                    "user_name": "작업자",
                    "accomplishment": 30,
                    "workdate": date_to_tz("2023-09-02T12:00:00+09:00"),
                },
                {
                    "gantt_id": 2,
                    "user_name": "어드민",
                    "accomplishment": 20,
                    "workdate": date_to_tz("2023-09-01T12:00:00+09:00"),
                },
                {
                    "gantt_id": 2,
                    "user_name": "작업자",
                    "accomplishment": 20,
                    "workdate": date_to_tz("2023-09-01T12:00:00+09:00"),
                },
                {
                    "gantt_id": 3,
                    "user_name": "어드민",
                    "accomplishment": 50,
                    "workdate": date_to_tz("2023-09-04T12:00:00+09:00"),
                },
                {
                    "gantt_id": 3,
                    "user_name": "작업자",
                    "accomplishment": 50,
                    "workdate": date_to_tz("2023-09-05T12:00:00+09:00"),
                },
                {
                    "gantt_id": 4,
                    "user_name": "작업자",
                    "accomplishment": 100,
                    "workdate": date_to_tz("2023-09-09T12:00:00+09:00"),
                },
                {
                    "gantt_id": 5,
                    "user_name": "작업자",
                    "accomplishment": 50,
                    "workdate": date_to_tz("2023-09-08T12:00:00+09:00"),
                },
                {
                    "gantt_id": 5,
                    "user_name": "어드민",
                    "accomplishment": 30,
                    "workdate": date_to_tz("2023-09-08T12:00:00+09:00"),
                },
                {
                    "gantt_id": 6,
                    "user_name": "작업자",
                    "accomplishment": 50,
                    "workdate": date_to_tz("2023-09-09T12:00:00+09:00"),
                },
                {
                    "gantt_id": 7,
                    "user_name": "작업자",
                    "accomplishment": 80,
                    "workdate": date_to_tz("2023-09-10T12:00:00+09:00"),
                },
                {
                    "gantt_id": 7,
                    "user_name": "어드민",
                    "accomplishment": 80,
                    "workdate": date_to_tz("2023-09-10T12:00:00+09:00"),
                },
                {
                    "gantt_id": 8,
                    "user_name": "작업자",
                    "accomplishment": 30,
                    "workdate": date_to_tz("2023-09-13T12:00:00+09:00"),
                },
                {
                    "gantt_id": 8,
                    "user_name": "어드민",
                    "accomplishment": 30,
                    "workdate": date_to_tz("2023-09-14T12:00:00+09:00"),
                },
                {
                    "gantt_id": 9,
                    "user_name": "작업자",
                    "accomplishment": 30,
                    "workdate": date_to_tz("2023-09-14T12:00:00+09:00"),
                },
                {
                    "gantt_id": 9,
                    "user_name": "어드민",
                    "accomplishment": 30,
                    "workdate": date_to_tz("2023-09-15T12:00:00+09:00"),
                },
                {
                    "gantt_id": 10,
                    "user_name": "작업자",
                    "accomplishment": 10,
                    "workdate": date_to_tz("2023-09-17T12:00:00+09:00"),
                },
                {
                    "gantt_id": 11,
                    "user_name": "어드민",
                    "accomplishment": 20,
                    "workdate": date_to_tz("2023-09-17T12:00:00+09:00"),
                },
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        print(f"Error: {e}")
        await session.rollback()
