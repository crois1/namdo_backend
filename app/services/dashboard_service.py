from app.dao import achievement_dao


# output achievement Detail data
async def output_master_dashboard(params, session):
    # 1. output achievement
    result = await achievement_dao.ReadAll_Achievement(
        params.name, session, params.start_date, params.end_date
    )

    # 2. return at success
    return result


# output total achievement data
async def output_total_accomplishment(params, session):
    # 1. output achievement
    result = await achievement_dao.Read_PlanAccomplishment(
        params.start_date,
        params.end_date,
        params.pdn,
        params.c,
        params.pdu,
        params.prn,
        params.fn,
        session,
    )

    # 2. return at success
    return result
