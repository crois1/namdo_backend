from fastapi import HTTPException, status

from app.services import gantt_service, plan_service
from app.dao import plan_dao, bom_dao, process_dao


# input bom data
async def input(state, payload, session):
    # 1. check process data validate
    await bom_dao.can_input_bom(payload, session)

    # 2. find plan data
    plan = await process_dao.Read(payload.process_name, session)

    # 3. update bom state
    await plan_dao.UpdateBomstate(plan, state, session)

    # 4. determine order
    bom_count = await bom_dao.CountBoms_Planid(payload.plan_id, session)
    payload.process_order = bom_count

    # 5. input bom
    result = await bom_dao.Create(payload, session)

    # 6. update plan state
    await plan_service.set_plan_state(plan, session)

    # 7. return at success
    return result


# output bom data
async def output(params, session):
    # 1. validate linked data
    await bom_dao.can_read_bom(params, session)

    # 2. output bom
    result = await bom_dao.ReadAll_Planid(params, session)  # return bom data

    if len(result) == 0:  # if none, create bom from recent product unit
        # find linked data
        plan = await plan_dao.Read(params, session)

        bom = await bom_dao.ReadPlanid_Unit(plan.product_unit, session)
        if bom is None:  # if no recent data exists, return no data
            return []

        process = await bom_dao.ReadAll_Planid(
            bom.plan_id, session
        )  # read recent BOM process

        result = await bom_dao.CreateAll(
            process, params, session
        )  # create all BOM process

    # 3. return at success
    return result


# edit bom data
async def edit(state, params, payload, session):
    # 1. find existing bom data & validate
    boms = await bom_dao.ReadAll_Planid(params, session)
    bom_ids = {bom.id for bom in boms}
    payload_set = set(payload)
    if not payload_set.issubset(bom_ids):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Invalid Process Data For BOM ID",
        )

    if len(bom_ids) != len(payload_set):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Invalid Process Data Amount",
        )

    # 2. find plan data
    plan = await plan_dao.Read(params, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Linked Plan Data"
        )

    # 3. update bom state
    await plan_dao.UpdateBomstate(plan, state, session)

    # 4. edit bom
    result = await bom_dao.Update(params, payload, session)

    # 5. update plan state
    await plan_service.set_plan_state(plan, session)

    # 6. return at success
    return result


# erase bom data
async def erase(state, params, session):
    # 1. check data validation
    result = await bom_dao.Read(params, session)
    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing BOM Data",
        )

    plan = await plan_dao.Read(result.plan_id, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Linked Plan Data"
        )

    # 2. delete linked gant datas
    await gantt_service.erase_all(params, session)

    # 3. erase bom
    await bom_dao.Delete(result, session)

    # 4. set bom state
    bom_count = await bom_dao.CountBoms_Planid(plan.id, session)
    if bom_count == 0:  # if BOM becomse none,
        state = "Undone"
    await plan_dao.UpdateBomstate(plan, state, session)

    # 5. set plan state
    await plan_service.set_plan_state(plan, session)

    # 6. return at success
    return result


# erase bom all data
async def erase_all(params, session, plan=None):
    # 1. Read plan for state change
    if plan is None:
        plan = await plan_dao.Read(params, session)
        if plan is None:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT, detail="No Plan Data"
            )

    # 2. find data & validate if nothing to erase
    results = await bom_dao.ReadAll_Planid(params, session)
    if len(results) == 0:
        return

    # 3. delete all bom
    await bom_dao.DeleteAll(params, session)

    # 4. delete linked gantt datas
    for bom in results:
        await gantt_service.erase_all(bom.id, session)

    # 5. set bom & plan state
    await plan_dao.UpdateBomstate(plan, "Undone", session)
    await plan_dao.UpdateState(plan, "Undone", session)

    # 6. return at success
    return results
