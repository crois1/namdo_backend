from fastapi import HTTPException, status
from datetime import datetime

from app.services import plan_service
from app.dao import user_dao, achievement_dao, bom_dao, plan_dao


# input achievement data
async def input(payload, session):
    # 1. check data validate
    user = await user_dao.Read_UserName(payload.user_name, session)

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing User Data",
        )
    plan = await plan_dao.Read_Ganttid(payload.gantt_id, session)

    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Linked Plan Data"
        )

    # 2. set workdate data by current time
    payload.workdate = datetime.now()

    # 3. input achievement data
    result = await achievement_dao.Create(payload, session)

    # 4. update plan state
    await plan_service.set_plan_state(plan, session)

    # 5. return at success
    return result


# output achievement Detail data
async def output_detail(params, session):
    # 1. output achievement
    result = await achievement_dao.ReadAll_Ganttid(params, session)

    # 2. return at success
    return result


# output accomplishment data
async def output_detail_accomplishment(params, session):
    # 1. check data validate

    plan = await plan_dao.Read_Ganttid(params, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Gant Data",
        )
    bom = await bom_dao.Read_Ganttid(params, session)
    if bom is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Gant Data",
        )

    # 2. find accomplishment
    result = plan.amount
    achievement_ratio = 0
    accomplishment = await achievement_dao.ReadAccomplishment_Bomid(
        bom.id, session
    )
    if accomplishment is not None:
        result -= accomplishment
        achievement_ratio = round(accomplishment / plan.amount * 100, 2)

    # 3. return at success
    return {
        "left_accomplishment": result,
        "achievement_ratio": achievement_ratio,
    }


# output achievement Master data
async def output_master(params, session):
    # 1. output achievement
    result = await achievement_dao.ReadAll_Username(params, session)

    # 2. return at success
    return result


# edit achievement accomplishment data
async def edit_accomplishment(payload, current_user, session):
    # 1. find data
    result = await achievement_dao.Read(payload.id, session)
    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Achievement Data",
        )

    # 2. check authority
    if current_user.name != result.user_name and current_user.role not in [
        "Master",
        "Admin",
    ]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient Privileges",
        )

    # 3. check data validate
    plan = await plan_dao.Read_Ganttid(result.gantt_id, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Linked Plan Data"
        )

    # 4. edit achievement
    result = await achievement_dao.UpdateAccomplishment(
        result, payload, session
    )

    # 5. update plan state
    await plan_service.set_plan_state(plan, session)

    # 6. return at success
    return result


# edit achievement data
async def edit_workdate(payload, current_user, session):
    # 1. find data
    result = await achievement_dao.Read(payload.id, session)
    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Achievement Data",
        )

    # 2. check authority
    if current_user.name != result.user_name and current_user.role not in [
        "Master",
        "Admin",
    ]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient Privileges",
        )

    # 3. edit achievement for workdate
    await achievement_dao.UpdateWorkdate(result, payload, session)

    # 4. return at success
    return result


# edit achievement data
async def edit_note(payload, current_user, session):
    # 1. find data
    result = await achievement_dao.Read(payload.id, session)
    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Achievement Data",
        )

    # 2. check authority
    if current_user.name != result.user_name and current_user.role not in [
        "Master",
        "Admin",
    ]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient Privileges",
        )

    # 3. edit achievement for workdate
    await achievement_dao.UpdateNote(result, payload, session)

    # 4. return at success
    return result


# erase achievement data
async def erase(params, current_user, session):
    # 1. find data
    result = await achievement_dao.Read(params, session)
    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Achievement Data",
        )

    # 2. check authority
    if current_user.name != result.user_name and current_user.role not in [
        "Master",
        "Admin",
    ]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient Privileges",
        )

    # 3. check data validate
    plan = await plan_dao.Read_Ganttid(result.gantt_id, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Linked Plan Data"
        )

    # 4. erase achievement
    await achievement_dao.Delete(result, session)

    # 5. update plan state
    await plan_service.set_plan_state(plan, session)

    # 6. return at success
    return result


# erase all achievement data via gantt
async def erase_all(gantt_id, session):
    # 1. delete
    await achievement_dao.DeleteAll(gantt_id, session)
    return
