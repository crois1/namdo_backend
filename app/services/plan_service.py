import random
from fastapi import HTTPException, status

from app.services import bom_service
from app.dao import gantt_dao, plan_dao, bom_dao, achievement_dao
from app.models.plan import Plan


# input plan data
async def input(payload, session):
    # 1. set background color
    payload.background_color = "#{:02x}{:02x}{:02x}".format(
        random.randint(125, 255),
        random.randint(125, 255),
        random.randint(125, 255),
    )

    # 2. input plan
    result = await plan_dao.Create(payload, session)

    # 3. return at success
    return result


# output plan data
async def output_admin(params, session):
    # 1. output plan
    result = await plan_dao.ReadAll_Date(params.madedate, session)

    # 2. return at success
    return result


# output plan data
async def output_detail(params, session):
    # 1. output plan
    result = await plan_dao.ReadAll_Period(
        params.start_date, params.end_date, session
    )

    # 2. return at success
    return result


# output plan data
async def output_detail_state(params, session):
    # 1. output plan
    result = await plan_dao.ReadState(params.id, session)

    # 2. return at success
    return result


async def output_detail_ganttdate(params, session):
    # 1. output plan
    result = await gantt_dao.ReadGantdate_Planid(params, session)

    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Gantt date to move Not Found",
        )

    # 2. return at success
    return result


async def output_plan_calendar(params, session):
    # 1. output plan
    result = await plan_dao.ReadCalendar(params, session)

    # 2. return at success
    return {"date": result}


# edit plan data
async def edit(payload, session):
    # 1. validate data
    await plan_dao.can_edit(payload.id, session)

    # 2. edit plan
    result = await plan_dao.Update(payload, session)

    # 3. return at success
    return result


# edit plan state data
async def edit_state(payload, session):
    # 1. find data
    result = await plan_dao.Read(payload.id, session)

    # 3. edit plan
    await plan_dao.UpdateState(result, "Done", session)

    # 4. return at success
    return result


# erase plan data
async def erase(params, session):
    # 1. find data
    result = await plan_dao.Read(params, session)

    # 3. delete linked bom datas
    await bom_service.erase_all(params, session, result)

    # 4. erase plan
    await plan_dao.Delete(result, session)

    # 5. return at success
    return result


# set plan state data
async def set_plan_state(plan: Plan, session):
    # 1. determine undone state (if bom is none, also return undone)
    if plan.bom_state != "Done":
        return await plan_dao.UpdateState(plan, "Undone", session)

    total_bomids = await bom_dao.ReadAllBomid_Plan(
        plan.id, session
    )  # [14, 10, 12]
    if len(total_bomids) == 0:
        return await plan_dao.UpdateState(plan, "Undone", session)

    # 2. determine editting state (if all bom is not on gantt chart)
    boms = await gantt_dao.CountBoms_Planid(plan.id, session)
    if boms is None or len(total_bomids) > boms:
        return await plan_dao.UpdateState(plan, "Editting", session)

    # 3. determine working state by last bom id
    plan_accomplishment = await achievement_dao.ReadAccomplishment_Bomid(
        total_bomids[0], session
    )
    if plan_accomplishment is None or plan.amount > plan_accomplishment:
        return await plan_dao.UpdateState(plan, "Working", session)

    # 4. determine Done state
    return await plan_dao.UpdateState(plan, "Done", session)
