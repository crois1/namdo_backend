from fastapi import HTTPException, status

from app.services import plan_service, achievement_service
from app.dao import facility_dao, gantt_dao, plan_dao, achievement_dao


# input gant data
async def input(payload, session):
    # 1. check data validate
    facility = await facility_dao.Read(payload.facility_name, session)

    if facility is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Facility Data",
        )
    plan = await plan_dao.Read_Bomid(payload.bom_id, session)

    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing BOM Data",
        )
    if plan.bom_state != "Done":  # bom state must be done before input
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="BOM State Not Done"
        )

    # 2. input gant
    result = await gantt_dao.Create(payload, session)

    # 3. update plan state
    await plan_service.set_plan_state(plan, session)

    # 4. return at success
    return result


# output gant data
async def output(params, session):
    # 1. output gant
    gantts = await gantt_dao.ReadAll_Date(
        params.start_date, params.end_date, session
    )

    result = []
    for gantt in gantts:
        # Read accomplishment Ratio
        accomplishment = await achievement_dao.ReadAccomplishment_Bomid(
            gantt.bom_id, session
        )
        achievement_ratio = 0
        if accomplishment is not None:
            achievement_ratio = round(accomplishment / gantt.amount * 100, 2)

        # Format Data
        gantt = {
            "id": gantt.id,
            "title": (
                f"{gantt.product_unit} - "
                f"{gantt.process_name} - "
                f"{gantt.amount}"
            ),
            "start_date": gantt.start_date,
            "end_date": gantt.end_date,
            "facility_name": gantt.facility_name,
            "background_color": gantt.background_color,
            "achievement_ratio": achievement_ratio,
        }
        result.append(gantt)

    # 2. return at success
    return result


async def output_gantt_calendar(params, session):
    # 1. output plan
    result = await gantt_dao.ReadCalendar(params, session)

    # 2. return at success
    return {"date": result}


# edit gant data
async def edit(payload, session):
    # 1. check and find data validate
    facility = await facility_dao.Read(payload.facility_name, session)

    if facility is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Facility Data",
        )
    result = await gantt_dao.Read(payload.id, session)

    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Gant Data",
        )

    # 2. edit gant
    await gantt_dao.Update(result, payload, session)

    # 3. return at success
    return result


# erase gant data
async def erase(params, session):
    # 1. find data
    result = await gantt_dao.Read(params, session)
    if result is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Gant Data",
        )

    plan = await plan_dao.Read_Bomid(result.bom_id, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Linked Plan Data"
        )

    # 2. delete linked achievement datas
    await achievement_service.erase_all(params, session)

    # 3. erase gant
    await gantt_dao.Delete(result, session)

    # 4. update plan state
    await plan_service.set_plan_state(plan, session)

    # 5. return at success
    return result


# erase all gant data via bom
async def erase_all(bom_id, session):
    # 1. find data
    gantt_ids = await gantt_dao.ReadAllGanttid_Bomid(bom_id, session)

    # 2. delete all gantts
    await gantt_dao.DeleteAll(bom_id, session)

    # 3. return if nothing to erase
    if len(gantt_ids) == 0:
        return

    # 4. delete linked achievement datas
    for gantt in gantt_ids:
        await achievement_service.erase_all(gantt.id, session)

    # 5. return at success
    return gantt_ids
