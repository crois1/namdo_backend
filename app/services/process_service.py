from app.dao import process_dao


# input process data
async def input(payload, session):
    # 1. check existing process data
    await process_dao.can_input_process(payload.process_name, session)

    # 2. input process
    result = await process_dao.Create(payload, session)

    # 3. return at success
    return result


# output process data
async def output(params, session):
    # 1. output process
    result = await process_dao.ReadAll(params, session)

    # 2. return at success
    return result


# edit process data
async def edit(payload, session):
    # 1. check data
    await process_dao.can_edit_process(payload, session)

    # 2. edit process
    result = await process_dao.Update(payload, session)

    # 3. return at success
    return result


# erase process data
async def erase(params, session):
    # 1. check data
    await process_dao.can_delete_process(params, session)

    # 3. erase process
    result = await process_dao.Delete(params, session)

    # 4. return at success
    return result
