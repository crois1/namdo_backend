import uuid
from fastapi import Response
from sqlalchemy.ext.asyncio import AsyncSession
from app.models.user import User
from app.dao import user_dao, auth_dao
from app.libs.hashUtil import hash_password, verify_password
from app.libs.tokenUtil import setToken


# input user
async def input_user(payload: User, session):
    # 1. check existing user data
    await user_dao.can_input_user(payload, session)

    # 2. hash password
    payload.pass_word = hash_password(payload.pass_word)

    # 3. input logic
    result = await user_dao.Create(payload, session)

    # 4. return at success
    return result


# login
async def login_user(
    payload: User,
    session: AsyncSession,
    response: Response,
) -> User:
    # 1. find user
    user = await user_dao.can_login_user(payload.user_id, session)

    # 2. verify password
    result = await user_dao.Read(user.id, session)
    verify_password(payload.pass_word, result.pass_word)

    # 3. Set Token & Response Header
    # device_uuid = str(uuid.uuid4())
    # await auth_dao.Update_Expire_OldDeviceToken(result.user_id, session)
    # result = await setToken(result, device_uuid, session)
    result = await setToken(result)

    response.headers["Access-Control-Allow-Headers"] = "Authorization"
    response.headers["authorization"] = (
        result["token_type"] + " " + result["access_token"]
    )

    # 4. return true
    return result


# output user
async def output_user(params, session):
    # 1. output user
    result = await user_dao.ReadAll(params, session)

    # 2. return at success
    return result


# output user name
async def output_user_name(params, session):
    # 1. output user
    result = await user_dao.ReadAllName(params, session)

    # 2. return at success
    return result


# edit user
async def edit_user(payload: User, session: AsyncSession):
    # 1. check and find existing data
    await user_dao.can_edit_user(payload, session)

    # 2. hash password data
    payload.pass_word = hash_password(payload.pass_word)

    # 3. edit user
    result = await user_dao.Update(payload, session)

    # 4. return at success
    return result


# erase user
async def erase_user(params, session):
    # 1. check existing user data
    await user_dao.can_erase_user(params, session)

    # 2. erase user
    result = await user_dao.Delete(params, session)

    # 2. return at success
    return result
