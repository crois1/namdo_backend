from app.dao import facility_dao


# input facility data
async def input(payload, session):
    # 1. check existing process data
    await facility_dao.can_input_facility(payload.facility_name, session)

    # 2. determine order
    Facilites = await facility_dao.ReadAll("", session)
    payload.order = len(Facilites)

    # 3. input facility
    result = await facility_dao.Create(payload, session)

    # 4. return at success
    return result


# output facility data
async def output(params, session):
    # 1. output facility
    result = await facility_dao.ReadAll(params, session)

    # 2. return at success
    return result


# edit facility name data
async def edit_name(payload, session):
    # 1. check data
    await facility_dao.can_edit_facility_name(payload, session)

    # 3. edit facility
    result = await facility_dao.UpdateName(payload, session)

    # 4. return at success
    return result


# edit facility order data
async def edit_order(payload, session):
    # 1. check data
    await facility_dao.can_edit_facility_order(payload, session)

    # 2. find data
    first = await facility_dao.Read(payload.first_facility_name, session)
    second = await facility_dao.Read(payload.second_facility_name, session)

    # 3. edit facility
    result = await facility_dao.UpdateOrder(first, second, session)

    # 4. return at success
    return result


# erase process data
async def erase(params, session):
    # 1. check data
    await facility_dao.can_delete_facility(params, session)

    # 2. find data
    result = await facility_dao.Read(params, session)

    # 3. erase process
    await facility_dao.Delete(result, session)

    # 4. return at success
    return result
