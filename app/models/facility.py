from sqlalchemy import Column, Integer, String, UniqueConstraint

from app.db import Base


# Facility Data
class Facility(Base):
    __tablename__ = "facility"

    id = Column(Integer, primary_key=True, index=True)
    facility_name = Column(String, unique=True, index=True, nullable=False)
    order = Column(Integer, nullable=False)

    __table_args__ = (
        UniqueConstraint("order", deferrable=True, initially="DEFERRED"),
    )
