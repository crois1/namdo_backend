from sqlalchemy import Column, Integer, String, DateTime

from app.db import Base


# Gantt Data
class Gantt(Base):
    __tablename__ = "gantt"

    # ID Info
    id = Column(Integer, primary_key=True, index=True)
    # Basic Info
    start_date = Column(DateTime(timezone=True), nullable=False)
    end_date = Column(DateTime(timezone=True), nullable=False)
    facility_name = Column(String, nullable=False)
    # Linked Data
    bom_id = Column(Integer, nullable=False, index=True)
