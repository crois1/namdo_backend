from sqlalchemy import Column, String, DateTime, Boolean, Integer
from sqlalchemy.dialects.postgresql import UUID

from app.db import Base


class Auth(Base):
    __tablename__ = "auth"

    id = Column(Integer, primary_key=True, nullable=False)
    uid = Column(String(40), nullable=False)
    did = Column(UUID(as_uuid=True), nullable=False)
    access_token = Column(String, nullable=False)
    expires_in = Column(DateTime(timezone=True), nullable=False)
    expired = Column(Boolean, nullable=False, default=False)
