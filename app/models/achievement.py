from sqlalchemy import Column, Integer, String, DateTime

from app.db import Base


# Achievement Data
class Achievement(Base):
    __tablename__ = "achievement"

    # ID Info
    id = Column(Integer, primary_key=True, index=True)
    # Basic Info
    user_name = Column(String, nullable=False, index=True)
    accomplishment = Column(Integer, nullable=False)
    workdate = Column(DateTime(timezone=True), nullable=False)
    note = Column(String)
    # Linked Data
    gantt_id = Column(Integer, nullable=False, index=True)
