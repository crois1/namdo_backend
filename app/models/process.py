from sqlalchemy import Column, Integer, String

from app.db import Base


# Process Data
class Process(Base):
    __tablename__ = "process"

    id = Column(Integer, primary_key=True, index=True)
    process_name = Column(String, unique=True, index=True, nullable=False)
