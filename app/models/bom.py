from sqlalchemy import Column, Integer, String
from app.db import Base


# BOM Data
class BOM(Base):
    __tablename__ = "bom"

    # ID Info
    id = Column(Integer, primary_key=True, index=True)
    # Basic Info
    process_name = Column(String, nullable=False)
    process_order = Column(Integer, nullable=False)
    # Linked Data
    plan_id = Column(Integer, index=True, nullable=False)
