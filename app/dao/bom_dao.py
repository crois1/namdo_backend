from fastapi import HTTPException, status
from sqlalchemy import (
    insert,
    select,
    update,
    delete,
    asc,
    desc,
    and_,
    func,
    case,
)
from app.dao import plan_dao, process_dao
from app.models.gantt import Gantt
from app.models.bom import BOM
from app.models.plan import Plan


# Check Existing BOM Data
async def can_input_bom(payload, session):
    process = await process_dao.Read(payload.process_name, session)
    if process is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Process Name",
        )
    plan = await plan_dao.Read(payload.plan_id, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Plan ID",
        )


async def can_read_bom(params, session):
    plan = await plan_dao.Read(params, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Plan ID",
        )


# Create BOM Data
async def Create(payload, session):
    # 1. Create BOM Data
    try:
        session.add(payload)
        await session.commit()
    except Exception as e:
        session.rollback()
        raise e
    else:
        await session.refresh(payload)

    # 2. Return at Success
    return payload


# Create All BOM Data(Copy)
async def CreateAll(payloads, params, session):
    # 1. Create All BOM Data
    try:
        query = insert(
            BOM,
        ).values(
            [
                {
                    "process_order": i.process_order,
                    "process_name": i.process_name,
                    "plan_id": params,
                }
                for i in payloads
            ]
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 2. Refresh & Return at Success
    return await ReadAll_Planid(params, session)


#  Read BOM Data by id
async def Read(params, session):
    # 1. Read BOM Data
    try:
        result = await session.get(BOM, params)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result


# Read BOM Data by Gant id
async def Read_Ganttid(params, session):
    # 1. Read BOM Data
    try:
        query = (
            select(
                BOM,
            )
            .join(
                Gantt,
                Gantt.bom_id == BOM.id,
            )
            .where(Gantt.id == params)
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalar_one_or_none()


# Read BOM Data by plan
async def ReadAll_Planid(params, session):
    # 1. Read BOM Data
    try:
        query = (
            select(
                BOM,
            )
            .filter_by(plan_id=params)
            .order_by(asc(BOM.process_order))
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalars().all()


# Read BOM Plan ID by Product Unit(BOM data must exist)
async def ReadPlanid_Unit(params, session):
    # 1. Read BOM Data
    try:
        query = (
            select(BOM.plan_id)
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .where(Plan.product_unit == params)
            .order_by(desc(BOM.id))
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.first()


# Read All BOM ID by BOM ID
async def ReadAllBomid_Plan(params, session):
    # 1. Read BOM Data
    try:
        query = (
            select(BOM.id)
            .where(BOM.plan_id == params)
            .order_by(
                desc(BOM.process_order),
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalars().all()


# Count Number of BOMS by Plan
async def CountBoms_Planid(params, session):
    # 1. Count BOMS
    try:
        query = (
            select(func.count(BOM.id))
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .where(
                Plan.id == params,
            )
            .group_by(
                Plan.id,
            )
        )
        result = await session.execute(query)
        result = result.scalar_one_or_none()
    except Exception as e:
        raise e

    # 2. Return at Success
    if result is None:
        return 0
    return result


# Update BOM Data
async def Update(params, payload, session):
    try:
        query = (
            update(BOM)
            .filter_by(plan_id=params)
            .values(
                process_order=case(
                    *[
                        (BOM.id == id, order)
                        for order, id in enumerate(payload)
                    ],
                )
            )
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 2. Return at Success
    return await ReadAll_Planid(params, session)


# Delete BOM Data
async def Delete(origin, session):
    try:
        # 1. Delete BOM Data
        await session.delete(origin)

        # 2. Find BOM Data to Re-Order
        query = (
            update(BOM)
            .where(
                and_(
                    BOM.plan_id == origin.plan_id,
                    BOM.process_order > origin.process_order,
                )
            )
            .values(process_order=BOM.process_order - 1)
        )
        await session.execute(query)

        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 3. Return at Success
    return origin


# Delete All BOM Data
async def DeleteAll(plan_id, session):
    try:
        query = delete(BOM).where(BOM.plan_id == plan_id)
        await session.execute(query)

        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    return plan_id
