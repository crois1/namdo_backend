from fastapi import HTTPException, status
from sqlalchemy import select, update, asc

from app.models.facility import Facility


# Check Existing Facility Data
async def can_input_facility(facility_name, session):
    facility_name = await Read(facility_name, session)
    if facility_name is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Existing Facility Name",
        )


async def can_edit_facility_name(payload, session):
    old_facility_name = await Read(payload.old_facility_name, session)
    if old_facility_name is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Facility Name",
        )
    new_facility_name = await Read(payload.new_facility_name, session)
    if new_facility_name is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Existing Facility Name",
        )


async def can_edit_facility_order(payload, session):
    first_facility = await Read(payload.first_facility_name, session)
    if first_facility is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing First Facility Name",
        )
    second_facility = await Read(payload.second_facility_name, session)
    if second_facility is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Second Facility Name",
        )


async def can_delete_facility(process_name, session):
    process_name = await Read(process_name, session)
    if process_name is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Process Name",
        )


# Create Facility Data
async def Create(params, session):
    # 1. Create Facility Data
    try:
        session.add(params)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(params)

    # 2. Return at Success
    return params


# Read All Facility Data
async def ReadAll(params, session):
    # 1. Read Facility Data
    try:
        query = (
            select(
                Facility,
            )
            .where(Facility.facility_name.like("%" + params + "%"))
            .order_by(asc(Facility.order))
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalars().all()


# Read Facility Data
async def Read(params, session):
    # 1. Read Facility Data
    try:
        query = select(
            Facility,
        ).filter_by(facility_name=params)
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalar_one_or_none()


# Update Facility Name Data
async def UpdateName(payload, session):
    # 1. Update Facility Name Data
    try:
        query = (
            update(Facility)
            .filter_by(facility_name=payload.old_facility_name)
            .values(
                facility_name=payload.new_facility_name,
            )
            .returning(Facility)
        )
        result = await session.execute(query)
        result = result.scalar_one_or_none()
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(result)

    # 2. Return at Success
    return result


# Update Facility Order Data
async def UpdateOrder(first, second, session):
    # 1. Update Facility Name Data
    try:
        temp = first.order
        first.order = second.order
        second.order = temp

        await session.commit()

    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(first)
        await session.refresh(second)

    # 2. Return at Success
    if first.order > second.order:
        return second, first
    else:
        return first, second


# Delete Facility Data
async def Delete(origin, session):
    try:
        # 1. Delete Facility Data
        await session.delete(origin)

        # 2. ReOrder
        query = (
            update(Facility)
            .where(Facility.order > origin.order)
            .values(order=Facility.order - 1)
        )
        await session.execute(query)

        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 3. Return at Success
    return origin
