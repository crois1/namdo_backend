from fastapi import HTTPException, status
from sqlalchemy import update, select, delete, asc

from app.models.process import Process


# Check Existing Process Data
async def can_input_process(process_name, session):
    process_name = await Read(process_name, session)
    if process_name is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Existing Process Name",
        )


async def can_edit_process(payload, session):
    old_process_name = await Read(payload.old_process_name, session)
    if old_process_name is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Process Name",
        )
    new_process_name = await Read(payload.new_process_name, session)
    if new_process_name is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Existing Process Name",
        )


async def can_delete_process(process_name, session):
    process_name = await Read(process_name, session)
    if process_name is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Process Name",
        )


# Create Process Data
async def Create(payload, session):
    # 1. Create Process Data
    try:
        session.add(payload)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(payload)

    # 2. Return at Success
    return payload


# Read Like Process Data
async def ReadAll(params, session):
    # 1. Read Process Data
    try:
        query = (
            select(Process)
            .where(Process.process_name.like("%" + params + "%"))
            .order_by(asc(Process.process_name))
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalars().all()


# Read Process Data
async def Read(params, session):
    # 1. Read Process Data
    try:
        query = select(
            Process,
        ).filter_by(process_name=params)
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalar_one_or_none()


# Update Process Data
async def Update(payload, session):
    # 1. Update Process Data
    try:
        query = (
            update(Process)
            .filter_by(process_name=payload.old_process_name)
            .values(
                process_name=payload.new_process_name,
            )
            .returning(Process)
        )
        result = await session.execute(query)
        result = result.scalar_one_or_none()
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(result)

    # 2. Return at Success
    return result


# Delete Process Data
async def Delete(params, session):
    # 1. Delete Process Data
    try:
        query = (
            delete(Process).filter_by(process_name=params).returning(Process)
        )
        result = await session.execute(query)
        result = result.scalar_one_or_none()
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 2. Return at Success
    return result
