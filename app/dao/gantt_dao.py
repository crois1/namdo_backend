from sqlalchemy.ext.asyncio import AsyncSession

from sqlalchemy import select, delete, and_, asc, func
from datetime import datetime, timedelta, timezone
from dateutil.relativedelta import relativedelta

from app.models.gantt import Gantt
from app.models.bom import BOM
from app.models.plan import Plan


# Create Gantt Data
async def Create(payload, session):
    # 1. Create Gantt Data
    try:
        session.add(payload)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(payload)

    # 2. Return at Success
    return payload


# Read Gantt Data by ID
async def Read(params, session):
    # 1. Read Gantt Data
    try:
        result = await session.get(Gantt, params)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result


# Read ID Plan Date Data
async def ReadGantdate_Planid(params, session: AsyncSession):
    # 1. Read Gantt Date Data
    try:
        query = (
            select(
                func.min(Gantt.start_date).label("date_to_move"),
            )
            .join(
                BOM,
                BOM.id == Gantt.bom_id,
            )
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .group_by(
                Plan.id,
            )
            .where(Plan.id == params)
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.one_or_none()


# Read All Gantt Data by BOM
async def ReadAll_Bomid(params, session):
    # 1. Read Gantt Data
    try:
        query = select(
            Gantt,
        ).where(Gantt.bom_id == params)
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalars().all()


# Read All Gantt ID by BOM ID
async def ReadAllGanttid_Bomid(params, session):
    # 1. Read Gantt Data
    try:
        query = select(
            Gantt.id,
        ).where(Gantt.bom_id == params)
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.all()


# Read Date Gantt Data
async def ReadAll_Date(params_start, params_end, session):
    # 1. Check Params
    if params_end is None:
        params_end = params_start + timedelta(days=31)
    else:
        params_end = params_end + timedelta(days=1)

    # 2. Read Gantt Data by Date
    try:
        query = (
            select(
                Gantt.id,
                Gantt.start_date,
                Gantt.end_date,
                Gantt.facility_name,
                BOM.id.label("bom_id"),
                BOM.process_name,
                Plan.product_unit,
                Plan.amount,
                Plan.background_color,
            )
            .join(
                BOM,
                BOM.id == Gantt.bom_id,
            )
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .group_by(
                Gantt.id,
                BOM.id,
                Plan.id,
            )
            .where(
                and_(
                    Gantt.end_date >= params_start,
                    Gantt.start_date < params_end,
                )
            )
            .order_by(
                asc(Gantt.start_date),
                asc(Gantt.end_date),
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 3. Return at Success
    return result.all()


# Read All BOM ID by BOM ID
async def CountBoms_Planid(params, session):
    # 1. Read BOM Data
    try:
        query = (
            select(
                func.count(BOM.id.distinct()).label("boms"),
            )
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .join(
                Gantt,
                Gantt.bom_id == BOM.id,
            )
            .where(
                Plan.id == params,
            )
            .group_by(
                Plan.id,
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalar_one_or_none()


async def ReadCalendar(params, session):
    # Extract year and month
    year = int(params[:4])
    month = int(params[4:6])

    # Define the start and end of the month
    start_of_month = datetime(
        year, month, 1, tzinfo=timezone(timedelta(hours=9))
    )
    end_of_month = start_of_month + relativedelta(months=1, days=-1)

    try:
        stmt = select(Gantt).where(
            and_(
                Gantt.start_date <= end_of_month,
                Gantt.end_date >= start_of_month,
            )
        )
        results = await session.execute(stmt)
        gantts = results.scalars().all()
        # return gantts
        # Generate unique timestamps set
        unique_timestamps = set()
        for entry in gantts:
            current_date = max(entry.start_date, start_of_month)
            end_date = min(entry.end_date, end_of_month)

            while current_date <= end_date:
                unique_timestamps.add(current_date)
                current_date += timedelta(days=1)
        # Convert set to sorted list
        return sorted(unique_timestamps)
    except Exception as e:
        raise e


# Update Gantt Data
async def Update(origin, payload, session):
    # 1. Update Gantt Data
    try:
        origin.start_date = payload.start_date
        origin.end_date = payload.end_date
        origin.facility_name = payload.facility_name
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(origin)

    # 2. Return at Success
    return origin


# Delete Gantt Data
async def Delete(origin, session):
    # 1. Delete Gantt Data
    try:
        await session.delete(origin)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 2. Return at Success
    return origin


# Delete All Gantt Data
async def DeleteAll(bom_id, session):
    # 1. Delete Gantt Data
    try:
        query = delete(Gantt).where(Gantt.bom_id == bom_id)
        await session.execute(query)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    return
