from fastapi import HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, update, asc, and_
from datetime import datetime, timedelta

from app.models.user import User


# Check
async def can_input_user(payload: User, session: AsyncSession) -> bool:
    # 1. Validate User Data
    user_id = await Read_UserID(payload.user_id, session)
    if user_id is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="user_id exist"
        )
    name = await Read_UserName(payload.name, session)
    if name is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="name exist"
        )
    email = await Read_Email(payload.email, session)
    if email is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="email exist"
        )
    return True


async def can_login_user(user_id: str, session: AsyncSession) -> User:
    # 1. find user
    result = await Read_UserID(user_id, session)
    if not result:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Matched User"
        )
    # 2. return user data at success
    return result


async def can_edit_user(payload: User, session: AsyncSession) -> bool:
    # 1. Validate User Data
    user_id = await Read_UserID(payload.user_id, session)
    if user_id is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="user_id exist"
        )
    name = await Read_UserName(payload.name, session)
    if name is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="name exist"
        )
    email = await Read_Email(payload.email, session)
    if email is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="email exist"
        )

    # 2. return user data at success
    return True


async def can_erase_user(id: int, session: AsyncSession) -> User:
    # 1. find user
    result = await Read(id, session)
    if not result or result.deletedAt is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="No Existing User"
        )
    # 2. return user data at success
    return result


# Create User Data
async def Create(payload, session: AsyncSession) -> User:
    # 1. Create Plan Data
    try:
        session.add(payload)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(payload)

    # 2. Return at Success
    return payload


# Read ID User Data
async def Read(params: int, session: AsyncSession) -> User:
    # 1. Read User Data
    try:
        result = await session.get(User, params)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result


# Read userid User Data
async def Read_UserID(params, session: AsyncSession):
    # 1. Read User Data
    try:
        query = select(
            User.id,
            User.user_id,
            User.name,
            User.email,
            User.role,
        ).filter_by(
            user_id=params,
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.one_or_none()


# Read name User Data
async def Read_UserName(params, session: AsyncSession):
    # 1. Read User Data
    try:
        query = select(
            User.id,
            User.user_id,
            User.name,
            User.email,
            User.role,
        ).filter_by(name=params)
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.one_or_none()


# Read email User Data
async def Read_Email(params, session: AsyncSession):
    # 1. Read User Data
    try:
        query = select(
            User.id,
            User.user_id,
            User.name,
            User.email,
            User.role,
        ).filter_by(email=params)
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.one_or_none()


# Read All User Data
async def ReadAll(params, session: AsyncSession):
    # 1. Read User Data
    try:
        query = (
            select(
                User.id,
                User.user_id,
                User.name,
                User.email,
                User.role,
            )
            .where(
                and_(
                    User.name.like("%" + params + "%"),
                    User.deletedAt.is_(None),
                )
            )
            .order_by(
                asc(User.name),
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.all()


# Read All User Name
async def ReadAllName(params, session: AsyncSession):
    # 1. Read User Data
    try:
        query = (
            select(
                User.name,
            )
            .where(
                and_(
                    User.name.like("%" + params + "%"),
                    User.deletedAt.is_(None),
                )
            )
            .order_by(
                asc(User.name),
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.all()


# Update User Data
async def Update(payload: User, session: AsyncSession):
    # 1. Update User Data
    try:
        query = (
            update(User)
            .filter_by(id=payload.id)
            .values(
                user_id=payload.user_id,
                pass_word=payload.pass_word,
                name=payload.name,
                email=payload.email,
                role=payload.role,
            )
            .returning(User)
        )
        result = await session.execute(query)
        result = result.scalar_one_or_none()
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(result)
    # 2. Return at Success
    return result


# Delete User Data
async def Delete(params, session: AsyncSession):
    # 1. Soft - Delete User Data
    try:
        query = (
            update(User)
            .filter_by(id=params)
            .values(deletedAt=datetime.now() + timedelta(days=360))
            .returning(User)
        )
        result = await session.execute(query)
        result = result.scalar_one_or_none()
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(result)

    # 2. Return at Success
    return result
