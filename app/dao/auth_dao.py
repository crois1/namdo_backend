from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, update, and_

from app.models.auth import Auth


# Create Blacklist Data
async def Create(payload: Auth, session: AsyncSession) -> Auth:
    # 1. Create Blacklist Data
    try:
        session.add(payload)
        await session.commit()
        await session.refresh(payload)

    except Exception as e:
        await session.rollback()
        raise e
    return payload


async def Read(uuid, access_token, session: AsyncSession) -> Auth:
    try:
        stmt = select(Auth).filter_by(
            did=uuid, access_token=access_token, expired=True
        )
        result = await session.execute(stmt)
    except Exception as e:
        raise e

    return result.scalar_one_or_none()


# Update Blacklist Data to Expire
async def Update_Expire_UsedToken(
    payload: Auth, session: AsyncSession
) -> Auth:
    # 1. Update Blacklist Data
    try:
        stmt = (
            update(Auth)
            .filter_by(did=payload.did, access_token=payload.access_token)
            .values(expired=True)
        )
        await session.execute(stmt)
        await session.commit()

    except Exception as e:
        await session.rollback()
        raise e

    return


# Update Blacklist Data to Expire
async def Update_Expire_OldDeviceToken(
    user_id: str, session: AsyncSession
) -> Auth:
    # 1. Update Blacklist Data
    try:
        stmt = (
            update(Auth).filter_by(uid=user_id)
            # .where(and_(Auth.uid == payload.uid, Auth.did != payload.did))
            .values(expired=True)
        )
        await session.execute(stmt)
        await session.commit()

    except Exception as e:
        await session.rollback()
        raise e

    return
