from fastapi import HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, update, asc
from datetime import datetime
from dateutil.relativedelta import relativedelta

from app.models.plan import Plan
from app.models.bom import BOM
from app.models.gantt import Gantt


async def can_edit(plan_id: int, session: AsyncSession):
    plan = await Read(plan_id, session)
    if plan is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="No Existing Plan Data",
        )


# Create Plan Data
async def Create(payload, session: AsyncSession):
    # 1. Create Plan Data
    try:
        session.add(payload)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(payload)

    # 2. Return at Success
    return payload


# Read Plan Data by ID
async def Read(params, session: AsyncSession):
    # 1. Read Plan Data
    try:
        result = await session.get(Plan, params)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result


# Read Plan Data by BOM ID
async def Read_Bomid(params, session: AsyncSession):
    # 1. Read BOM Data
    try:
        query = (
            select(
                Plan,
            )
            .join(
                BOM,
                BOM.plan_id == Plan.id,
            )
            .where(BOM.id == params)
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalar_one_or_none()


# Read Plan Data by Gantt ID
async def Read_Ganttid(params, session: AsyncSession):
    # 1. Read Gantt Data
    try:
        query = (
            select(
                Plan,
            )
            .join(
                BOM,
                BOM.plan_id == Plan.id,
            )
            .join(
                Gantt,
                Gantt.bom_id == BOM.id,
            )
            .where(Gantt.id == params)
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalar_one_or_none()


# Read Date Plan Data
async def ReadAll_Date(params, session: AsyncSession):
    # 1. Read Plan Data
    try:
        query = (
            select(
                Plan.id,
                Plan.company,
                Plan.lot,
                Plan.material_unit,
                Plan.material_amount,
                Plan.product_name,
                Plan.product_unit,
                Plan.amount,
                Plan.deadline,
                Plan.note,
            )
            .filter_by(madedate=params)
            .order_by(
                asc(Plan.company),
                asc(Plan.product_name),
                asc(Plan.product_unit),
                asc(Plan.amount),
            )
        )
        result = await session.execute(query)

    except Exception as e:
        raise e

    # 2. Return at Success
    return result.all()


# Read Period Plan Data
async def ReadAll_Period(params_start, params_end, session: AsyncSession):
    # 1. Read Plan Data
    try:
        query = (
            select(
                Plan.id,
                Plan.state,
                Plan.company,
                Plan.lot,
                Plan.product_name,
                Plan.product_unit,
                Plan.amount,
                Plan.deadline,
                Plan.bom_state,
                Plan.background_color,
            )
            .where(Plan.madedate.between(params_start, params_end))
            .order_by(
                asc(Plan.madedate),
                asc(Plan.company),
                asc(Plan.product_name),
                asc(Plan.product_unit),
                asc(Plan.amount),
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.all()


# Read ID Plan State Data
async def ReadState(params, session: AsyncSession):
    # 1. Read Plan Data
    try:
        query = select(
            Plan.id,
            Plan.state,
            Plan.bom_state,
        ).filter_by(id=params)
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.one_or_none()


async def ReadCalendar(params, session: AsyncSession):
    # 1. Read Plan Data
    year = params // 100
    month = params % 100
    # print(year, month)
    start_date = datetime(year, month, 1)
    end_date = start_date + relativedelta(months=1) - relativedelta(days=1)

    try:
        stmt = (
            select(Plan.madedate)
            .where(Plan.madedate.between(start_date, end_date))
            .distinct()
            .order_by(Plan.madedate.asc())
        )

        results = await session.execute(stmt)
        plans = results.scalars().all()
        return plans
    except Exception as e:
        raise e


# Update Plan Data
async def Update(payload: Plan, session: AsyncSession):
    # 1. Update Plan Data
    try:
        query = (
            update(Plan)
            .filter_by(id=payload.id)
            .values(
                company=payload.company,
                lot=payload.lot,
                material_unit=payload.material_unit,
                material_amount=payload.material_amount,
                product_name=payload.product_name,
                product_unit=payload.product_unit,
                amount=payload.amount,
                deadline=payload.deadline,
                note=payload.note,
            )
            .returning(Plan)
        )
        result = await session.execute(query)
        result = result.scalar_one_or_none()
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(result)

    # 2. Return at Success
    return result


# Update Plan State Data
async def UpdateState(params, new_state, session: AsyncSession):
    # 1. Update Plan Data
    try:
        params.state = new_state
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(params)

    # 2. Return at Success
    return params


# Update BOM State Data
async def UpdateBomstate(params, new_bom_state, session: AsyncSession):
    # 1. Update Plan Data
    try:
        params.bom_state = new_bom_state
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(params)

    # 2. Return at Success
    return params


# Delete Plan Data
async def Delete(origin, session: AsyncSession):
    # 1. Delete Plan Data
    try:
        await session.delete(origin)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 2. Return at Success
    return origin
