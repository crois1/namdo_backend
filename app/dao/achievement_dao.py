from sqlalchemy import select, delete, func, asc, desc, and_

from datetime import timedelta

from app.models.achievement import Achievement
from app.models.gantt import Gantt
from app.models.bom import BOM
from app.models.plan import Plan


# Create Achievement Data
async def Create(payload, session):
    # 1. Create Achievement Data
    try:
        session.add(payload)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(payload)

    # 2. Return at Success
    return payload


# Read Achievement Data by ID
async def Read(params, session):
    # 1. Read Achievement Data
    try:
        result = await session.get(Achievement, params)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result


# Read BOM Accomplishment
async def ReadAccomplishment_Bomid(params, session):
    # 1. Read BOM Accomplishment Data
    try:
        query = (
            select(
                func.sum(Achievement.accomplishment).label("accomplishment"),
            )
            .join(
                Gantt,
                Gantt.id == Achievement.gantt_id,
            )
            .join(
                BOM,
                BOM.id == Gantt.bom_id,
            )
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .where(
                BOM.id == params,
            )
            .group_by(
                BOM.id,
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalar_one_or_none()


# Read Plan Accomplishment
async def Read_PlanAccomplishment(
    start_date,
    end_date,
    product_name,
    company,
    product_unit,
    process_name,
    facility_name,
    session,
):
    # Read Plan Accomplishment Data
    try:
        # 1. Read Plan Achievement Process(Last BOM Process)
        subquery = (
            select(
                Plan.id,
                func.max(BOM.process_order).label("last_process_order"),
            )
            .join(
                BOM,
                BOM.plan_id == Plan.id,
            )
            .group_by(Plan.id)
            .subquery()
        )

        # 2. Read Plan Datas
        query = (
            select(
                Plan.product_name,
                Plan.company,
                Plan.product_unit,
                BOM.process_name,
                Gantt.facility_name,
                Achievement.workdate,
                Achievement.accomplishment,
            )
            .join(
                Gantt,
                Gantt.id == Achievement.gantt_id,
            )
            .join(
                BOM,
                BOM.id == Gantt.bom_id,
            )
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .join(
                subquery,
                and_(
                    subquery.c.id == Plan.id,
                    subquery.c.last_process_order == BOM.process_order,
                ),
            )
            .where(
                Achievement.workdate.between(
                    start_date, end_date + timedelta(days=1)
                )
            )
        )

        # 3. Check Filter Options
        if product_name:
            query = query.filter(
                Plan.product_name == product_name,
            )
        if company:
            query = query.filter(
                Plan.company == company,
            )
        if product_unit:
            query = query.filter(
                Plan.product_unit == product_unit,
            )
        if process_name:
            query = query.filter(
                BOM.process_name == process_name,
            )
        if facility_name:
            query = query.filter(
                Gantt.facility_name == facility_name,
            )

        # 4. Output Result With workdate order
        query = query.order_by(
            asc(Achievement.workdate),
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 5. Return at Success
    return result.all()


# Read Achievement Data by gant_id
async def ReadAll_Ganttid(params, session):
    # 1. Read Achievement Data
    try:
        query = (
            select(
                Achievement,
            )
            .where(Achievement.gantt_id == params)
            .order_by(
                asc(Achievement.workdate),
            )
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.scalars().all()


# Read Achievement Data by user_name
async def ReadAll_Username(params, session):
    # 1. Read Achievement Data
    try:
        query = (
            select(
                Achievement.id,
                Achievement.user_name,
                Achievement.accomplishment,
                Achievement.workdate,
                Achievement.note,
                Gantt.facility_name,
                BOM.process_name,
                Plan.product_unit,
                Plan.company,
                Plan.product_name,
            )
            .join(
                Gantt,
                Gantt.id == Achievement.gantt_id,
            )
            .join(
                BOM,
                BOM.id == Gantt.bom_id,
            )
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .where(
                Achievement.user_name == params,
            )
            .order_by(desc(Achievement.workdate))
        )
        result = await session.execute(query)
    except Exception as e:
        raise e

    # 2. Return at Success
    return result.all()


# Read Achievement Data by date
async def ReadAll_Achievement(name, session, start_date=None, end_date=None):
    # 1. Read Achievement Data
    try:
        query = (
            select(
                Achievement.id,
                Achievement.user_name,
                Achievement.accomplishment,
                Achievement.workdate,
                Gantt.facility_name,
                BOM.process_name,
                Plan.product_unit,
                Plan.company,
                Plan.lot,
                Plan.product_name,
            )
            .join(
                Gantt,
                Gantt.id == Achievement.gantt_id,
            )
            .join(
                BOM,
                BOM.id == Gantt.bom_id,
            )
            .join(
                Plan,
                Plan.id == BOM.plan_id,
            )
            .where(Achievement.user_name.like("%" + name + "%"))
        )

        if start_date is not None and end_date is not None:
            query = query.where(
                Achievement.workdate.between(
                    start_date, end_date + timedelta(days=1)
                )
            )
        query = query.order_by(desc(Achievement.workdate))

        result = await session.execute(query)

    except Exception as e:
        raise e

    # 2. Return at Success
    return result.all()


# Update Achievement accomplishment Data
async def UpdateAccomplishment(origin, payload, session):
    # 1. Update Achievement accomplishment Data
    try:
        origin.accomplishment = payload.accomplishment
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(origin)

    # 2. Return at Success
    return origin


# Update Achievement workdate Data
async def UpdateWorkdate(origin, payload, session):
    # 1. Update Achievement workdate Data
    try:
        origin.workdate = payload.workdate
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(origin)

    # 2. Return at Success
    return origin


# Update Achievement note Data
async def UpdateNote(origin, payload, session):
    # 1. Update Achievement workdate Data
    try:
        origin.note = payload.note
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e
    else:
        await session.refresh(origin)

    # 2. Return at Success
    return origin


# Delete Achievement Data
async def Delete(origin, session):
    # 1. Delete Achievement Data
    try:
        await session.delete(origin)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 2. Return at Success
    return origin


# Delete All Achievement Data
async def DeleteAll(gantt_id, session):
    # 1. Delete Achievement Data
    try:
        query = delete(
            Achievement,
        ).where(
            Achievement.gantt_id == gantt_id,
        )
        await session.execute(query)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise e

    # 2. Return at Success
    return
