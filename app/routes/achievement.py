import yaml

with open(
    "app/static/route_descriptions/achievement.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import APIRouter, HTTPException, status, Body, Path, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List, Annotated

from app.db import postgresql
from app.models.user import User
from app.models.achievement import Achievement
from app.dto.achievement import (
    Achievement_Create_Request,
    Achievement_Create_Response,
    AchievementDetail_Read_Response,
    AchievementDetail_ReadAccomplishment_Response,
    AchievementMaster_Read_Response,
    Achievement_UpdateAccomplishment_Request,
    Achievement_UpdateAccomplishment_Response,
    Achievement_UpdateWorkdate_Request,
    Achievement_UpdateWorkdate_Response,
    Achievement_UpdateNote_Request,
    Achievement_UpdateNote_Response,
    Achievement_Delete_Response,
)
from app.services import achievement_service
from app.libs.authUtil import check_Master, check_User

router = APIRouter(
    prefix="/achievement",
    dependencies=[Depends(check_User)],
    tags=["/achievement"],
)


# create achievement data
@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=Achievement_Create_Response,
    description=description_data["ACHIEVEMENT_POST"]["DESCRIPTION"],
    response_description=description_data["ACHIEVEMENT_POST"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def achievement_create(
    request: Annotated[
        Achievement_Create_Request,
        Body(examples=description_data["ACHIEVEMENT_POST"]["EXAMPLES"]),
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_create")
    # 1. Check Request
    try:
        payload = Achievement(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await achievement_service.input(payload, session)

    # 3. Response
    return result


# read achievement data
@router.get(
    "/detail/{gantt_id}",
    status_code=status.HTTP_200_OK,
    response_model=List[AchievementDetail_Read_Response],
    description=description_data["ACHIEVEMENT_DETAIL_GET"]["DESCRIPTION"],
    response_description=description_data["ACHIEVEMENT_DETAIL_GET"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def achievement_read_detail(
    gantt_id: Annotated[int, Path(description="Gantt ID to Read Data from")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_read_detail")
    # 1. Execute Business Logic
    result = await achievement_service.output_detail(
        params=gantt_id, session=session
    )

    # 2. Reponse
    return result


# read accomplishment data
@router.get(
    "/detail/{gantt_id}/accomplishment",
    status_code=status.HTTP_200_OK,
    response_model=AchievementDetail_ReadAccomplishment_Response,
    description=description_data["ACHIEVEMENT_DETAIL_GET_ACCOMPLISHMENT"][
        "DESCRIPTION"
    ],
    response_description=description_data[
        "ACHIEVEMENT_DETAIL_GET_ACCOMPLISHMENT"
    ]["RESPONSE_DESCRIPTION"],
)
async def achievement_read_detail_accomplishment(
    gantt_id: Annotated[int, Path(description="Gantt ID to Read Data from")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_read_detail_accomplishment")
    # 1. Execute Business Logic
    result = await achievement_service.output_detail_accomplishment(
        params=gantt_id, session=session
    )

    # 2. Reponse
    return result


# read Master achievement data
@router.get(
    "/master/{user_name}",
    status_code=status.HTTP_200_OK,
    response_model=List[AchievementMaster_Read_Response],
    dependencies=[Depends(check_Master)],
    description=description_data["ACHIEVEMENT_MASTER_GET"]["DESCRIPTION"],
    response_description=description_data["ACHIEVEMENT_MASTER_GET"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def achievement_read_master(
    user_name: Annotated[
        str, Path(description="User Name to Read Achievement")
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_read_master")
    # 1. Execute Business Logic
    result = await achievement_service.output_master(
        params=user_name, session=session
    )

    # 3. Reponse
    return result


# update achievement accomplishment data
@router.put(
    "/accomplishment",
    status_code=status.HTTP_200_OK,
    response_model=Achievement_UpdateAccomplishment_Response,
    description=description_data["ACHIEVEMENT_PUT_ACCOMPLISHMENT"][
        "DESCRIPTION"
    ],
    response_description=description_data["ACHIEVEMENT_PUT_ACCOMPLISHMENT"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def achievement_update_accomplishment(
    current_user: Annotated[User, Depends(check_User)],
    request: Annotated[Achievement_UpdateAccomplishment_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_update_accomplishment")
    # 1. Check Request
    try:
        payload = Achievement(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await achievement_service.edit_accomplishment(
        payload, current_user, session
    )

    # 3. Response
    return result


# update achievement workdate data
@router.put(
    "/workdate",
    status_code=status.HTTP_200_OK,
    response_model=Achievement_UpdateWorkdate_Response,
    description=description_data["ACHIEVEMENT_PUT_WORKDATE"]["DESCRIPTION"],
    response_description=description_data["ACHIEVEMENT_PUT_WORKDATE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def achievement_update_workdate(
    current_user: Annotated[User, Depends(check_User)],
    request: Annotated[Achievement_UpdateWorkdate_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_update_workdate")
    # 1. Check Request
    try:
        payload = Achievement(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await achievement_service.edit_workdate(
        payload, current_user, session
    )

    # 3. Response
    return result


# update achievement note data
@router.put(
    "/note",
    status_code=status.HTTP_200_OK,
    response_model=Achievement_UpdateNote_Response,
    description=description_data["ACHIEVEMENT_PUT_NOTE"]["DESCRIPTION"],
    response_description=description_data["ACHIEVEMENT_PUT_NOTE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def achievement_update_note(
    current_user: Annotated[User, Depends(check_User)],
    request: Annotated[Achievement_UpdateNote_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_update_note")
    # 1. Check Request
    try:
        payload = Achievement(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await achievement_service.edit_note(
        payload, current_user, session
    )

    # 3. Response
    return result


# delete achievement data
@router.delete(
    "/{id}",
    status_code=status.HTTP_200_OK,
    response_model=Achievement_Delete_Response,
    description=description_data["ACHIEVEMENT_DELETE"]["DESCRIPTION"],
    response_description=description_data["ACHIEVEMENT_DELETE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def achievement_delete(
    current_user: Annotated[User, Depends(check_User)],
    id: Annotated[int, Path(description="Achievement ID for Delete")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("achievement_delete")
    # 1. Execute Business Logic
    result = await achievement_service.erase(
        params=id, current_user=current_user, session=session
    )

    # 2. Response
    return result
