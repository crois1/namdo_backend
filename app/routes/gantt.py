import yaml

with open(
    "app/static/route_descriptions/gantt.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import (
    APIRouter,
    HTTPException,
    status,
    Body,
    Path,
    Query,
    Depends,
)
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List, Annotated

from app.db import postgresql
from app.models.gantt import Gantt
from app.dto.gantt import (
    Gantt_Create_Request,
    Gantt_Create_Response,
    Gantt_Read_Parameters,
    Gantt_Read_Response,
    Gantt_Update_Request,
    Gantt_Update_Response,
    Gantt_Delete_Response,
    Gantt_Calendar_Response,
)
from app.services import gantt_service
from app.libs.authUtil import check_User

router = APIRouter(
    prefix="/gantt",
    dependencies=[Depends(check_User)],
    tags=["/gantt"],
)


# create gantt data
@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=Gantt_Create_Response,
    description=description_data["GANTT_POST"]["DESCRIPTION"],
    response_description=description_data["GANTT_POST"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def gantt_create(
    request: Annotated[
        Gantt_Create_Request,
        Body(examples=description_data["GANTT_POST"]["EXAMPLES"]),
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("gantt_create")
    # 1. Check Request
    try:
        payload = Gantt(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await gantt_service.input(payload, session)

    # 3. Response
    return result


# read gantt data
@router.get(
    "/{start_date}",
    status_code=status.HTTP_200_OK,
    response_model=List[Gantt_Read_Response],
    description=description_data["GANTT_GET"]["DESCRIPTION"],
    response_description=description_data["GANTT_GET"]["RESPONSE_DESCRIPTION"],
)
async def gantt_read(
    start_date: Annotated[str, Path(description="Start Date to Search")],
    end_date: Annotated[
        str | None, Query(description="End Date to Search")
    ] = None,
    session: AsyncSession = Depends(postgresql.connect),
):
    print("gantt_read")
    # 1. Check Request
    try:
        params = Gantt_Read_Parameters(
            start_date=start_date,
            end_date=end_date,
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await gantt_service.output(params, session)

    # 3. Reponse
    return result


@router.get(
    "/calendar/{year_month}",
    status_code=status.HTTP_200_OK,
    response_model=Gantt_Calendar_Response,
    description=description_data["GANTT_GET"]["DESCRIPTION"],
    response_description=description_data["GANTT_GET"]["RESPONSE_DESCRIPTION"],
)
async def gantt_calendar(
    year_month: Annotated[str, Path(description="YYYYMM Year Month data")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("gantt_calendar")
    result = await gantt_service.output_gantt_calendar(year_month, session)
    return result


# update gantt data
@router.put(
    "",
    status_code=status.HTTP_200_OK,
    response_model=Gantt_Update_Response,
    description=description_data["GANTT_PUT"]["DESCRIPTION"],
    response_description=description_data["GANTT_PUT"]["RESPONSE_DESCRIPTION"],
)
async def gantt_update(
    request: Annotated[Gantt_Update_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("gantt_update")
    # 1. Check Request
    try:
        payload = Gantt(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await gantt_service.edit(payload, session)

    # 3. Response
    return result


# delete gantt data
@router.delete(
    "/{id}",
    status_code=status.HTTP_200_OK,
    response_model=Gantt_Delete_Response,
    description=description_data["GANTT_DELETE"]["DESCRIPTION"],
    response_description=description_data["GANTT_DELETE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def gantt_delete(
    id: Annotated[int, Path(description="Gantt ID for Delete")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("gantt_delete")
    # 1. Execute Business Logic
    result = await gantt_service.erase(params=id, session=session)

    # 2. Response
    return result
