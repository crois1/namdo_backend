import yaml

with open(
    "app/static/route_descriptions/user.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import (
    APIRouter,
    HTTPException,
    status,
    Body,
    Path,
    Query,
    Depends,
)
from typing import List, Annotated
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import postgresql
from app.models.user import User
from app.dto.user import (
    User_Create_Request,
    User_Create_Response,
    User_ReadAll_Response,
    User_ReadAllNames_Response,
    User_Update_Request,
    User_Update_Response,
    User_Delete_Response,
)
from app.libs.authUtil import check_Master, check_User
from app.services import user_service

router = APIRouter(
    prefix="/user",
    dependencies=[Depends(check_User)],
    tags=["/user"],
)


# create user
@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=User_Create_Response,
    dependencies=[Depends(check_Master)],
    description=description_data["USER_POST"]["DESCRIPTION"],
    response_description=description_data["USER_POST"]["RESPONSE_DESCRIPTION"],
)
async def user_create(
    request: Annotated[
        User_Create_Request,
        Body(examples=description_data["USER_POST"]["EXAMPLES"]),
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("user_create")
    # 1. Check Request
    try:
        payload = User(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await user_service.input_user(payload, session)

    # 3. Response
    return result


# read user
@router.get(
    "",
    status_code=status.HTTP_200_OK,
    response_model=List[User_ReadAll_Response],
    dependencies=[Depends(check_Master)],
    description=description_data["USER_GET"]["DESCRIPTION"],
    response_description=description_data["USER_GET"]["RESPONSE_DESCRIPTION"],
)
async def user_read_all(
    name: Annotated[str | None, Query(description="Name for Get")] = "",
    session: AsyncSession = Depends(postgresql.connect),
):
    print("user_read_all")
    # 1. Execute Business Logic
    result = await user_service.output_user(params=name, session=session)

    # 2. Response
    return result


# read users' name
@router.get(
    "/name",
    status_code=status.HTTP_200_OK,
    response_model=List[User_ReadAllNames_Response],
    description=description_data["USER_GET_NAME"]["DESCRIPTION"],
    response_description=description_data["USER_GET_NAME"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def user_read_all_names(
    name: Annotated[str | None, Query(description="Name for Get")] = "",
    session: AsyncSession = Depends(postgresql.connect),
):
    print("user_read_all_names")
    # 1. Execute Business Logic
    result = await user_service.output_user_name(params=name, session=session)

    # 2. Response
    return result


# update user
@router.put(
    "",
    status_code=status.HTTP_200_OK,
    response_model=User_Update_Response,
    dependencies=[Depends(check_Master)],
    description=description_data["USER_PUT"]["DESCRIPTION"],
    response_description=description_data["USER_PUT"]["RESPONSE_DESCRIPTION"],
)
async def user_update(
    request: Annotated[User_Update_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("user_update")
    # 1. Check Request
    try:
        payload = User(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await user_service.edit_user(payload, session)

    # 3. Response
    return result


# delete user data
@router.delete(
    "/{id}",
    status_code=status.HTTP_200_OK,
    response_model=User_Delete_Response,
    dependencies=[Depends(check_Master)],
    description=description_data["USER_DELETE"]["DESCRIPTION"],
    response_description=description_data["USER_DELETE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def user_delete(
    id: Annotated[int, Path(description="User ID for Delete")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("user_delete")
    # 1. Execute Business Logic
    result = await user_service.erase_user(params=id, session=session)

    # 2. Response
    return result


# # get background image
# @router.get("/kingdom/user/background")
# async def read_image():
#     # return responses.FileResponse(
#     #     f"static/images/home.jpg", filename="home.jpg"
#     # )
#     return responses.FileResponse(
#         f"static/images/home.gif", filename="home.gif"
#     )
