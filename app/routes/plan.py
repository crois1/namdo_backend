import yaml

with open(
    "app/static/route_descriptions/plan.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import (
    APIRouter,
    HTTPException,
    status,
    Body,
    Path,
    Header,
    Depends,
)
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List, Annotated

from app.db import postgresql
from app.models.plan import Plan
from app.dto.plan import (
    Plan_Create_Request,
    Plan_Create_Response,
    PlanAdmin_Read_Parameters,
    PlanAdmin_Read_Response,
    PlanDetail_Read_Parameters,
    PlanDetail_Read_Response,
    Plan_ReadState_Response,
    Plan_ReadGanttDate_Response,
    Plan_Calendar_Response,
    Plan_Update_Request,
    Plan_Update_Response,
    Plan_UpdateState_Request,
    Plan_UpdateState_Response,
    Plan_Delete_Response,
)
from app.services import plan_service
from app.libs.authUtil import check_Admin, check_User

from datetime import datetime
from zoneinfo import ZoneInfo

router = APIRouter(
    prefix="/plan",
    dependencies=[Depends(check_User)],
    tags=["/plan"],
)


# create plan data
@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=Plan_Create_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["PLAN_POST"]["DESCRIPTION"],
    response_description=description_data["PLAN_POST"]["RESPONSE_DESCRIPTION"],
)
async def plan_create(
    request: Annotated[
        Plan_Create_Request,
        Body(examples=description_data["PLAN_POST"]["EXAMPLES"]),
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_create")
    # 1. Check Request
    try:
        payload = Plan(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await plan_service.input(payload, session)

    # 3. Response
    return result


# read Admin plan data
@router.get(
    "/date/{madedate}",
    status_code=status.HTTP_200_OK,
    response_model=List[PlanAdmin_Read_Response],
    dependencies=[Depends(check_Admin)],
    description=description_data["PLAN_ADMIN_GET"]["DESCRIPTION"],
    response_description=description_data["PLAN_ADMIN_GET"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def plan_read_admin(
    madedate: Annotated[str, Path(description="Plan Madedate")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_read_admin")
    # 1. Check Request
    try:
        params = PlanAdmin_Read_Parameters(madedate=madedate)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await plan_service.output_admin(params, session)

    # 3. Reponse
    return result


# read Detail plan data
@router.get(
    "/date/{start_date}/{end_date}",
    status_code=status.HTTP_200_OK,
    response_model=List[PlanDetail_Read_Response],
    description=description_data["PLAN_DETAIL_GET"]["DESCRIPTION"],
    response_description=description_data["PLAN_DETAIL_GET"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def plan_detail(
    start_date: Annotated[str, Path(description="Start Date to Read")],
    end_date: Annotated[str, Path(description="End Date to Read")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_detail")
    # 1. Check Request
    try:
        params = PlanDetail_Read_Parameters(
            start_date=start_date,
            end_date=end_date,
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await plan_service.output_detail(params, session)

    # 3. Reponse
    return result


# read Detail plan state data
@router.get(
    "/state/{id}",
    status_code=status.HTTP_200_OK,
    response_model=Plan_ReadState_Response | None,
    description=description_data["PLAN_DETAIL_GET_STATE"]["DESCRIPTION"],
    response_description=description_data["PLAN_DETAIL_GET_STATE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def plan_detail_state(
    id: Annotated[int, Path(description="Plan ID for Get")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_detail_state")
    # 1. Check Request
    try:
        params = Plan(id=id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await plan_service.output_detail_state(params, session)

    # 3. Reponse
    return result


# read Detail plan gantt data
@router.get(
    "/ganttdate/{id}",
    status_code=status.HTTP_200_OK,
    response_model=Plan_ReadGanttDate_Response,
    description=description_data["PLAN_DETAIL_GET_GANTTID"]["DESCRIPTION"],
    response_description=description_data["PLAN_DETAIL_GET_GANTTID"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def plan_detail_gantt(
    id: Annotated[int, Path(description="Plan ID for Get")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_detail_gantt")
    # 1. Execute Business Logic
    result = await plan_service.output_detail_ganttdate(
        params=id, session=session
    )

    # 2. Reponse
    return result


# read Calendar plan data
@router.get(
    "/calendar/{year_month}",
    status_code=status.HTTP_200_OK,
    response_model=Plan_Calendar_Response,
    # description=description_data["PLAN_DETAIL_GET_GANTTID"]["DESCRIPTION"],
    # response_description=description_data["PLAN_DETAIL_GET_GANTTID"][
    #     "RESPONSE_DESCRIPTION"
    # ],
)
async def plan_calendar(
    year_month: Annotated[int, Path(description="YYYYMM Year Month data")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_calendar")
    # 1. Execute Business Logic
    result = await plan_service.output_plan_calendar(
        params=year_month, session=session
    )

    # 2. Reponse
    return result


# update plan data
@router.put(
    "",
    status_code=status.HTTP_200_OK,
    response_model=Plan_Update_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["PLAN_PUT"]["DESCRIPTION"],
    response_description=description_data["PLAN_PUT"]["RESPONSE_DESCRIPTION"],
)
async def plan_update(
    request: Annotated[Plan_Update_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_update")
    # 1. Check Request
    try:
        payload = Plan(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await plan_service.edit(payload, session)

    # 3. Response
    return result


# update plan state data forcely
@router.put(
    "/state",
    status_code=status.HTTP_200_OK,
    response_model=Plan_UpdateState_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["PLAN_PUT_STATE"]["DESCRIPTION"],
    response_description=description_data["PLAN_PUT_STATE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def plan_update_state(
    request: Annotated[Plan_UpdateState_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_update_state")
    # 1. Check Request
    try:
        payload = Plan(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await plan_service.edit_state(payload, session)

    # 3. Response
    return result


# delete plan data
@router.delete(
    "/{id}",
    status_code=status.HTTP_200_OK,
    response_model=Plan_Delete_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["PLAN_DELETE"]["DESCRIPTION"],
    response_description=description_data["PLAN_DELETE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def plan_delete(
    id: Annotated[int, Path(description="Plan ID for Delete")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("plan_delete")
    # 1. Execute Business Logic
    result = await plan_service.erase(params=id, session=session)

    # 2. Response
    return result
