import yaml

with open(
    "app/static/route_descriptions/auth.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import APIRouter, HTTPException, status, Response, Body, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Annotated

from app.db import postgresql
from app.models.user import User
from app.dto.auth import (
    AuthLogin_Response,
    AuthSignUp_Request,
    AuthSignUp_Response,
)
from app.services import user_service

router = APIRouter(
    prefix="/auth",
    tags=["/auth"],
)


# login
@router.post(
    "/login",
    status_code=status.HTTP_201_CREATED,
    response_model=AuthLogin_Response,
    description=description_data["AUTH_POST_LOGIN"]["DESCRIPTION"],
    response_description=description_data["AUTH_POST_LOGIN"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def user_login(
    request: Annotated[OAuth2PasswordRequestForm, Depends()],
    response: Response,
    session: AsyncSession = Depends(postgresql.connect),
):
    print("user_login")
    # 1. Check Request
    try:
        payload = User(
            user_id=request.username,
            pass_word=request.password,
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Bussiness Logic
    result = await user_service.login_user(payload, session, response)

    # 4. Response
    return result


# signup
@router.post(
    "/signup",
    status_code=status.HTTP_201_CREATED,
    response_model=AuthSignUp_Response,
    description=description_data["AUTH_POST_SIGNUP"]["DESCRIPTION"],
    response_description=description_data["AUTH_POST_SIGNUP"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def user_signup(
    request: Annotated[AuthSignUp_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("user_signup")
    # 1. Check Request
    try:
        payload = User(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await user_service.input_user(payload, session)

    # 3. Response
    return result
