import yaml

with open(
    "app/static/route_descriptions/facility.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import (
    APIRouter,
    HTTPException,
    status,
    Body,
    Path,
    Query,
    Depends,
)
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List, Annotated

from app.db import postgresql
from app.models.facility import Facility
from app.dto.facility import (
    Facility_Create_Request,
    Facility_Create_Response,
    Facility_Read_Response,
    Facility_UpdateName_Request,
    Facility_UpdateName_Response,
    Facility_UpdateOrder_Request,
    Facility_UpdateOrder_Response,
    Facility_Delete_Response,
)
from app.services import facility_service
from app.libs.authUtil import check_Admin, check_User

router = APIRouter(
    prefix="/facility",
    dependencies=[Depends(check_User)],
    tags=["/facility"],
)


# create facility data
@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=Facility_Create_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["FACILITY_POST"]["DESCRIPTION"],
    response_description=description_data["FACILITY_POST"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def facility_create(
    request: Annotated[
        Facility_Create_Request,
        Body(examples=description_data["FACILITY_POST"]["EXAMPLES"]),
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("facility_create")
    # 1. Check Request
    try:
        payload = Facility(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await facility_service.input(payload, session)

    # 3. Response
    return result


# read facility data
@router.get(
    "",
    status_code=status.HTTP_200_OK,
    response_model=List[Facility_Read_Response],
    description=description_data["FACILITY_GET"]["DESCRIPTION"],
    response_description=description_data["FACILITY_GET"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def facility_read(
    facility_name: Annotated[
        str | None, Query(description="Facility Name for Get")
    ] = "",
    session: AsyncSession = Depends(postgresql.connect),
):
    print("facility_read")
    # 1. Execute Business Logic
    result = await facility_service.output(
        params=facility_name, session=session
    )

    # 2. Reponse
    return result


# update facility name data
@router.put(
    "/name",
    status_code=status.HTTP_200_OK,
    response_model=Facility_UpdateName_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["FACILITY_PUT_NAME"]["DESCRIPTION"],
    response_description=description_data["FACILITY_PUT_NAME"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def facility_update_name(
    request: Annotated[Facility_UpdateName_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("facility_update_name")
    # 1. Check Request
    result = await facility_service.edit_name(request, session)

    # 3. response
    return result


# update facility order data
@router.put(
    "/order",
    status_code=status.HTTP_200_OK,
    response_model=List[Facility_UpdateOrder_Response],
    dependencies=[Depends(check_Admin)],
    description=description_data["FACILITY_PUT_ORDER"]["DESCRIPTION"],
    response_description=description_data["FACILITY_PUT_ORDER"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def facility_update_order(
    request: Annotated[Facility_UpdateOrder_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("facility_update_order")
    # 1. Check Request
    result = await facility_service.edit_order(request, session)

    # 3. response
    return result


# delete facility data
@router.delete(
    "/{facility_name}",
    status_code=status.HTTP_200_OK,
    response_model=Facility_Delete_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["FACILITY_DELETE"]["DESCRIPTION"],
    response_description=description_data["FACILITY_DELETE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def facility_delete(
    facility_name: Annotated[
        str, Path(description="Facility Name for Delete")
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("facility_delete")
    # 1. Execute Business
    response = await facility_service.erase(
        params=facility_name, session=session
    )

    # 2. response
    return response
