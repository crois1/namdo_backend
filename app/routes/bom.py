import yaml

with open(
    "app/static/route_descriptions/bom.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import (
    APIRouter,
    HTTPException,
    status,
    Body,
    Path,
    Query,
    Depends,
)
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List, Annotated

from app.db import postgresql
from app.models.bom import BOM
from app.dto.bom import (
    BOM_Create_Request,
    BOM_Create_Response,
    BOM_Read_Response,
    BOM_Update_Request,
    BOM_Update_Response,
    BOMState,
    BOM_Delete_Response,
    BOM_DeleteAll_Response,
)
from app.services import bom_service
from app.libs.authUtil import check_Admin, check_User

router = APIRouter(
    prefix="/bom",
    dependencies=[Depends(check_User)],
    tags=["/bom"],
)


# create BOM data
@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=BOM_Create_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["BOM_POST"]["DESCRIPTION"],
    response_description=description_data["BOM_POST"]["RESPONSE_DESCRIPTION"],
)
async def bom_create(
    request: Annotated[
        BOM_Create_Request,
        Body(examples=description_data["BOM_POST"]["EXAMPLES"]),
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("bom_create")
    # 1. Check Request
    try:
        payload = BOM(
            plan_id=request.plan_id,
            process_name=request.process_name,
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    if request.bom_state != "Editting":
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bad Request: Can Only Edit in Editting State",
        )

    # 2. Execute Business Logic
    result = await bom_service.input(request.bom_state, payload, session)

    # 3. Response
    return result


# read BOM data
@router.get(
    "/{plan_id}",
    status_code=status.HTTP_200_OK,
    response_model=List[BOM_Read_Response],
    description=description_data["BOM_GET"]["DESCRIPTION"],
    response_description=description_data["BOM_GET"]["RESPONSE_DESCRIPTION"],
)
async def bom_read(
    plan_id: Annotated[int, Path(description="Linked Plan ID")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("bom_read")
    # 1. Execute Business Logic
    result = await bom_service.output(params=plan_id, session=session)

    # 2. Reponse
    return result


# update BOM data
@router.put(
    "",
    status_code=status.HTTP_200_OK,
    response_model=List[BOM_Update_Response],
    dependencies=[Depends(check_Admin)],
    description=description_data["BOM_PUT"]["DESCRIPTION"],
    response_description=description_data["BOM_PUT"]["RESPONSE_DESCRIPTION"],
)
async def bom_update(
    request: Annotated[BOM_Update_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("bom_update")
    # 1. Check Request
    if request.bom_state != "Editting" and request.bom_state != "Done":
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bad Request: Can't Edit On Undone State",
        )

    # 2. Execute Business Logic
    result = await bom_service.edit(
        state=request.bom_state,
        params=request.plan_id,
        payload=request.process_list,
        session=session,
    )

    # 3. Response
    return result


# delete BOM data
@router.delete(
    "/{id}",
    status_code=status.HTTP_200_OK,
    response_model=BOM_Delete_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["BOM_DELETE"]["DESCRIPTION"],
    response_description=description_data["BOM_DELETE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def bom_delete(
    id: Annotated[int, Path(description="BOM ID for Delete")],
    bom_state: Annotated[BOMState, Query(description="Check BOM State")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("bom_delete")
    # 1. Check Request
    if bom_state != "Editting":
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bad Request: Can Only Edit in Editting State",
        )

    # 2. Execute Business Logic
    result = await bom_service.erase(
        state=bom_state, params=id, session=session
    )

    # 3. Response
    return result


# delete All BOM data
@router.delete(
    "/all/{plan_id}",
    status_code=status.HTTP_200_OK,
    response_model=List[BOM_DeleteAll_Response] | None,
    dependencies=[Depends(check_Admin)],
    description=description_data["BOM_DELETE_ALL"]["DESCRIPTION"],
    response_description=description_data["BOM_DELETE_ALL"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def bom_delete_all(
    plan_id: Annotated[int, Path(description="Plan ID for Delete All")],
    bom_state: Annotated[BOMState, Query(description="Check BOM State")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("bom_delete_all")
    # 1. Check Request
    if bom_state != "Editting":
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bad Request: Can Only Edit in Editting State",
        )

    # 2. Execute Business Logic
    result = await bom_service.erase_all(params=plan_id, session=session)

    # 3. Response
    return result
