import yaml

with open(
    "app/static/route_descriptions/process.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import (
    APIRouter,
    HTTPException,
    status,
    Body,
    Path,
    Query,
    Depends,
)
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List, Annotated

from app.db import postgresql
from app.models.process import Process
from app.dto.process import (
    Process_Create_Request,
    Process_Create_Response,
    Process_Read_Response,
    Process_Update_Request,
    Process_Update_Response,
    Process_Delete_Response,
)
from app.services import process_service
from app.libs.authUtil import check_Admin, check_User

router = APIRouter(
    prefix="/process",
    dependencies=[Depends(check_User)],
    tags=["/process"],
)


# create process data
@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=Process_Create_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["PROCESS_POST"]["DESCRIPTION"],
    response_description=description_data["PROCESS_POST"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def process_create(
    request: Annotated[
        Process_Create_Request,
        Body(examples=description_data["PROCESS_POST"]["EXAMPLES"]),
    ],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("process_create")
    # 1. Check Request
    try:
        payload = Process(**request.model_dump())
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await process_service.input(payload, session)

    # 3. Response
    return result


# read process data
@router.get(
    "",
    status_code=status.HTTP_200_OK,
    response_model=List[Process_Read_Response],
    description=description_data["PROCESS_GET"]["DESCRIPTION"],
    response_description=description_data["PROCESS_GET"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def process_read(
    process_name: Annotated[
        str | None, Query(description="Process Name for Get")
    ] = "",
    session: AsyncSession = Depends(postgresql.connect),
):
    print("process_read")
    # 1. Execute Business Logic
    result = await process_service.output(params=process_name, session=session)

    # 2. Reponse
    return result


# update process data
@router.put(
    "",
    status_code=status.HTTP_200_OK,
    response_model=Process_Update_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["PROCESS_PUT"]["DESCRIPTION"],
    response_description=description_data["PROCESS_PUT"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def process_update(
    request: Annotated[Process_Update_Request, Body()],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("process_update")
    # 1. Execute Business
    result = await process_service.edit(request, session)

    # 2. response
    return result


# delete process data
@router.delete(
    "/{process_name}",
    status_code=status.HTTP_200_OK,
    response_model=Process_Delete_Response,
    dependencies=[Depends(check_Admin)],
    description=description_data["PROCESS_DELETE"]["DESCRIPTION"],
    response_description=description_data["PROCESS_DELETE"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def process_delete(
    process_name: Annotated[str, Path(description="Process Name for Delete")],
    session: AsyncSession = Depends(postgresql.connect),
):
    print("process_delete")
    # 1. Execute Business
    result = await process_service.erase(params=process_name, session=session)

    # 2. response
    return result
