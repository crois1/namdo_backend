import yaml

with open(
    "app/static/route_descriptions/dashboard.yaml", "r", encoding="utf-8"
) as yaml_file:
    description_data = yaml.safe_load(yaml_file)

from fastapi import (
    APIRouter,
    HTTPException,
    status,
    Path,
    Query,
    Depends,
)
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List, Annotated

from app.db import postgresql
from app.dto.dashboard import (
    Dashboard_ParametersTotal,
    Dashboard_ResponseTotal,
    DashboardMaster_Parameters,
    Dashboard_Master_Response,
)
from app.services import dashboard_service
from app.libs.authUtil import check_Master, check_User

router = APIRouter(
    prefix="/dashboard",
    dependencies=[Depends(check_User)],
    tags=["/dashboard"],
)


# read achievement data
@router.get(
    "/master",
    status_code=status.HTTP_200_OK,
    response_model=List[Dashboard_Master_Response],
    dependencies=[Depends(check_Master)],
    description=description_data["DASHBOARD_MASTER_GET"]["DESCRIPTION"],
    response_description=description_data["DASHBOARD_MASTER_GET"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def dashboard_master(
    name: Annotated[str | None, Query(description="User Name for Read")] = "",
    start_date: Annotated[
        str | None, Query(description="Search Date from start")
    ] = None,
    end_date: Annotated[
        str | None, Query(description="Search Date until end")
    ] = None,
    session: AsyncSession = Depends(postgresql.connect),
):
    print("dashboard_master")
    # 1. Check Request
    try:
        params = DashboardMaster_Parameters(
            name=name,
            start_date=start_date,
            end_date=end_date,
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Bad Request: {str(e)}",
        )

    # 2. Execute Business Logic
    result = await dashboard_service.output_master_dashboard(params, session)

    # 3. Reponse
    return result


# read achievement dashboard data
@router.get(
    "/total/accomplishment/{start_date}/{end_date}",
    status_code=status.HTTP_200_OK,
    response_model=List[Dashboard_ResponseTotal],
    description=description_data["DASHBOARD_GET_TOTAL"]["DESCRIPTION"],
    response_description=description_data["DASHBOARD_GET_TOTAL"][
        "RESPONSE_DESCRIPTION"
    ],
)
async def dashboard_total_accomplishment(
    start_date: Annotated[str, Path(description="Search Date from start")],
    end_date: Annotated[str, Path(description="Search Date untill end")],
    pdn: Annotated[
        str | None, Query(description="Product Name for Filter")
    ] = "",
    c: Annotated[
        str | None, Query(description="Company Name for Filter")
    ] = "",
    pdu: Annotated[
        str | None, Query(description="Product Unit for Filter")
    ] = "",
    prn: Annotated[
        str | None, Query(description="Process Name for Filter")
    ] = "",
    fn: Annotated[
        str | None, Query(description="Facility Name for Filter")
    ] = "",
    session: AsyncSession = Depends(postgresql.connect),
):
    print("dashboard_total_accomplishment")
    # 1. Check Request
    try:
        params = Dashboard_ParametersTotal(
            start_date=start_date,
            end_date=end_date,
            c=c,
            pdn=pdn,
            pdu=pdu,
            prn=prn,
            fn=fn,
        )
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Bad Request: {str(e)}")

    # 2. Execute Business Logic
    response = await dashboard_service.output_total_accomplishment(
        params, session
    )

    # 3. Reponse
    return response
