from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    POSTGRESQL_URL: str = (
        # "postgresql+asyncpg://postgres:82721911@localhost:5432/namdo"
        # "postgresql+asyncpg://postgres:82721911@192.168.0.41:5432/namdo"
        "postgresql+asyncpg://crois:crois!23@pg-geuo8.vpc-pub-cdb-kr.ntruss.com:5432/namdo"
        # "postgresql+asyncpg://crois:crois!23@pg-geuo8.vpc-pub-cdb-kr.ntruss.com:5432/namdo_dev"
    )
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 2
    SECRET_KEY: str = "cookiesecret"
    HASH_SCHEMA: str = "bcrypt"
    ALGORITHM: str = "HS256"
    TOKEN_TYPE: str = "Bearer"
    TEST_POSTGRESQL_URL: str = (
        "postgresql+asyncpg://postgres:82721911@localhost:5432/test_namdo"
    )


settings = Settings()
