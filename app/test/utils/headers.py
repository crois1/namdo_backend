from httpx import AsyncClient


async def master_login_header(client: AsyncClient):
    response = await client.post(
        "/auth/login",
        data={"username": "Master", "password": "Master"},
    )
    token_type = response.json()["token_type"]
    access_token = response.json()["access_token"]
    headers = {"Authorization": f"{token_type} {access_token}"}
    return headers


async def admin_login_header(client: AsyncClient):
    response = await client.post(
        "/auth/login",
        data={"username": "Admin", "password": "Admin"},
    )
    token_type = response.json()["token_type"]
    access_token = response.json()["access_token"]
    headers = {"Authorization": f"{token_type} {access_token}"}
    return headers


async def worker_login_header(client: AsyncClient):
    response = await client.post(
        "/auth/login",
        data={"username": "Worker", "password": "Worker"},
    )
    token_type = response.json()["token_type"]
    access_token = response.json()["access_token"]
    headers = {"Authorization": f"{token_type} {access_token}"}
    return headers
