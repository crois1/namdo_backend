import pytest
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import text, select, insert

from .conftest import engine
from app.libs.hashUtil import hash_password
from app.db import Base


@pytest.mark.asyncio
async def test_create_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)
        result = await conn.execute(
            text(
                "SELECT count(*) FROM information_schema.tables WHERE table_schema = 'namdo'"
            )
        )
        table_count = result.scalar()
        assert table_count == 8


@pytest.mark.asyncio
async def test_master_user_data(session: AsyncSession):
    from app.models.user import User

    query = insert(User).values(
        [
            {
                "user_id": "Master",
                "pass_word": hash_password("Master"),
                "name": "마스터",
                "email": "Master@master.com",
                "role": "Master",
            },
            {
                "user_id": "Admin",
                "pass_word": hash_password("Admin"),
                "name": "어드민",
                "email": "Admin@admin.com",
                "role": "Admin",
            },
            {
                "user_id": "Worker",
                "pass_word": hash_password("Worker"),
                "name": "작업자",
                "email": "Worker@worker.com",
                "role": "Worker",
            },
        ]
    )
    await session.execute(query)
    await session.commit()

    query = select(User).where(User.user_id == "Master")
    result = await session.execute(query)

    assert result.scalars().first().user_id == "Master"
