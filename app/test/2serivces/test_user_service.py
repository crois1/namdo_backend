from fastapi import HTTPException, status
from unittest.mock import patch, Mock, AsyncMock
import pytest
from pytest_mock import MockerFixture
from sqlalchemy.ext.asyncio import AsyncSession

from app.dao import user_dao
from app.services import user_service
from app.libs.hashUtil import hash_password, verify_password
from app.models.user import User
from app.services.user_service import (
    input_user,
    login_user,
    output_user,
    output_user_name,
    edit_user,
    erase_user,
)


@pytest.mark.asyncio
@patch("app.dao.user_dao.Create", new_callable=AsyncMock)
async def test_user_input(
    mock_create: AsyncMock,
    mocker: MockerFixture,
    session: AsyncSession,
):
    # Arrange
    input = User(
        user_id="Worker1",
        pass_word="Worker1",
        name="워커1",
        email="Worker1@worker.com",
        role="Worker",
    )

    spy_can_input_user = mocker.spy(user_dao, "can_input_user")
    spy_hash_password = mocker.spy(user_service, "hash_password")

    # Act
    # with pytest.raises(HTTPException) as exc_info:
    await input_user(input, session)

    # Assert
    # assert str(exc_info.value.detail) != "user_id exist"
    # assert exc_info.value.status_code != status.HTTP_404_NOT_FOUND
    assert spy_can_input_user.call_count == 1
    assert spy_hash_password.call_count == 1
    assert mock_create.call_count == 1

    hashed_password = spy_hash_password.spy_return
    assert verify_password("Worker1", hashed_password)


@pytest.mark.asyncio
async def test_user_login(
    mocker: MockerFixture,
    session: AsyncSession,
):
    # Arrange
    input = User(
        user_id="Worker",
        pass_word="Worker",
    )

    spy_can_login_user = mocker.spy(user_dao, "can_login_user")
    spy_Read = mocker.spy(user_dao, "Read")
    spy_verify_password = mocker.spy(user_service, "verify_password")
    setToken = mocker.spy(user_service, "setToken")

    mock_response = Mock()
    mock_response.headers = {}

    # Act
    result = await login_user(input, session, mock_response)

    # Assert
    assert (
        mock_response.headers["Access-Control-Allow-Headers"]
        == "Authorization"
    )
    assert result["token_type"] == "Bearer"
    assert result["user_id"] == "Worker"
    assert spy_can_login_user.call_count == 1
    assert spy_Read.call_count == 1
    assert spy_verify_password.call_count == 1
    assert setToken.call_count == 1


@pytest.mark.asyncio
async def test_user_output(
    mocker: MockerFixture,
    session: AsyncSession,
):
    # Arrange
    input = "어"

    spy_ReadAll = mocker.spy(user_dao, "ReadAll")

    # Act
    result = await output_user(input, session)

    # Assert
    assert result[0].user_id == "Admin"
    assert spy_ReadAll.call_count == 1


@pytest.mark.asyncio
async def test_user_output_name(
    mocker: MockerFixture,
    session: AsyncSession,
):
    # Arrange
    input = "어"

    spy_ReadAllName = mocker.spy(user_dao, "ReadAllName")

    # Act
    result = await output_user_name(input, session)

    # Assert
    assert result[0][0] == "어드민"
    assert spy_ReadAllName.call_count == 1


@pytest.mark.asyncio
@patch("app.dao.user_dao.Update", new_callable=AsyncMock)
async def test_user_edit(
    mock_update: AsyncMock,
    mocker: MockerFixture,
    session: AsyncSession,
):
    # Arrange
    input = User(
        id=2,
        user_id="Worker1",
        pass_word="Worker1",
        name="워커1",
        email="Worker1@worker.com",
        role="Worker",
    )

    spy_can_edit_user = mocker.spy(user_dao, "can_edit_user")
    spy_hash_password = mocker.spy(user_service, "hash_password")

    # Act
    await edit_user(input, session)

    # Assert
    assert spy_can_edit_user.call_count == 1
    assert spy_hash_password.call_count == 1
    assert mock_update.call_count == 1

    hashed_password = spy_hash_password.spy_return
    assert verify_password("Worker1", hashed_password)


@pytest.mark.asyncio
@patch("app.dao.user_dao.Delete", new_callable=AsyncMock)
async def test_user_erase(
    mock_delete: AsyncMock,
    mocker: MockerFixture,
    session: AsyncSession,
):
    # Arrange
    input = 1

    spy_can_erase_user = mocker.spy(user_dao, "can_erase_user")

    # Act
    await erase_user(input, session)

    # Assert
    assert spy_can_erase_user.call_count == 1
    assert mock_delete.call_count == 1
