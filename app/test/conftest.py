import asyncio
import pytest
import pytest_asyncio
from httpx import AsyncClient

from typing import Dict, AsyncGenerator, Generator
from sqlalchemy.ext.asyncio import (
    create_async_engine,
    async_sessionmaker,
    AsyncSession,
    AsyncEngine,
)

from app.main import app
from app.config import settings
from .utils.headers import master_login_header
from app.db import Base, postgresql

# from asgi_lifespan import LifespanManager

# from test_config import test_postgresql

engine = create_async_engine(
    settings.TEST_POSTGRESQL_URL,
    pool_pre_ping=True,
)

async_session = async_sessionmaker(
    bind=engine,
    autoflush=False,
    autocommit=False,
    expire_on_commit=False,
)


@pytest_asyncio.fixture(scope="session")
async def session():
    async with async_session() as s:
        yield s


@pytest_asyncio.fixture(scope="session")
async def test_app() -> Generator:
    async def override_get_db():
        async with async_session() as session:
            yield session

    app.dependency_overrides[postgresql.connect] = override_get_db
    yield app


@pytest_asyncio.fixture(scope="session")
async def client(test_app) -> AsyncGenerator:
    # async with LifespanManager(test_app):
    async with AsyncClient(app=test_app, base_url="http://test") as c:
        yield c


@pytest_asyncio.fixture(scope="function")
async def master_token_headers(client: AsyncClient) -> Dict[str, str]:
    return await master_login_header(client)


@pytest.fixture(scope="session")
def event_loop():
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()
