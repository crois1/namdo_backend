import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from app.libs.hashUtil import hash_password, verify_password
from app.models.user import User
from app.dao.user_dao import (
    Create,
    Read,
    Read_UserID,
    Read_UserName,
    Read_Email,
    ReadAll,
    ReadAllName,
    Update,
    Delete,
)


@pytest.mark.asyncio
async def test_user_create(session: AsyncSession):
    # Arrange
    new_user = User(
        user_id="Admin1",
        pass_word=hash_password("Admin1"),
        name="어드민1",
        email="Admin1@admin.com",
        role="Admin",
    )

    # Act
    result = await Create(new_user, session)

    # Assert
    assert result.user_id == "Admin1"
    assert result.pass_word != "Admin"
    verify_password("Admin1", result.pass_word)


@pytest.mark.asyncio
async def test_user_read(session: AsyncSession):
    params = 4
    result = await Read(params, session)
    assert result.id == 4
    verify_password("Admin1", result.pass_word)


@pytest.mark.asyncio
async def test_user_read_id(session: AsyncSession):
    params = "Admin1"
    result = await Read_UserID(params, session)
    assert result.user_id == "Admin1"


@pytest.mark.asyncio
async def test_user_read_username(session: AsyncSession):
    params = "어드민1"
    result = await Read_UserName(params, session)
    assert result.name == "어드민1"


@pytest.mark.asyncio
async def test_user_read_email(session: AsyncSession):
    params = "Admin1@admin.com"
    result = await Read_Email(params, session)
    assert result.email == "Admin1@admin.com"


@pytest.mark.asyncio
async def test_user_read_all(session: AsyncSession):
    params = "어"
    result = await ReadAll(params, session)
    assert len(result) == 2


@pytest.mark.asyncio
async def test_user_read_all_name(session: AsyncSession):
    params = "어"
    result = await ReadAllName(params, session)
    assert len(result) == 2


@pytest.mark.asyncio
async def test_user_update(session: AsyncSession):
    new_user = User(
        id=4,
        user_id="camel",
        pass_word=hash_password("camel"),
        name="낙타",
        email="Camel@camel.com",
        role="Worker",
    )
    result = await Update(new_user, session)
    assert result.id == 4
    assert result.user_id == "camel"


@pytest.mark.asyncio
async def test_user_delete(session: AsyncSession):
    params = 4
    result = await Delete(params, session)
    assert result.id == 4
    assert result.user_id == "camel"
