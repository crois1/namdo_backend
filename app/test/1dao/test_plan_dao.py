import random
from datetime import datetime
import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from app.models.plan import Plan
from app.dao.plan_dao import (
    Create,
    Read,
    Read_Bomid,
    Read_Ganttid,
    ReadAll_Date,
    ReadAll_Period,
    ReadState,
    Update,
    UpdateState,
    UpdateBomstate,
    Delete,
)


# @pytest.mark.asyncio
# async def test_plan_create(session: AsyncSession):
#     # Arrange
#     new_plan = Plan(
#         madedate=datetime.fromisoformat("2023-07-01T00:00:01+09:00"),
#         company="TestCompany",
#         lot="Lot123",
#         material_unit="kg",
#         material_amount="50",
#         product_name="TestProduct",
#         product_unit="pcs",
#         amount=100,
#         deadline="2023-09-01",  # Adjust this based on your model's definition
#         note="Test note",
#         background_color="#{:02x}{:02x}{:02x}".format(
#             random.randint(125, 255),
#             random.randint(125, 255),
#             random.randint(125, 255),
#         ),
#     )

#     # Act
#     result = await Create(new_plan, session)

#     # Assert
#     assert result.company == "TestCompany"
#     assert result.product_name == "TestProduct"


# @pytest.mark.asyncio
# async def test_plan_read(session: AsyncSession):
#     # Arrange
#     plan_id = 1  # This should be an ID that exists in your test database
#     result = await Read(plan_id, session)

#     # Assert
#     assert result.id == plan_id
# ... add other assertions based on expected values


# @pytest.mark.asyncio
# async def test_plan_read_bomid(session: AsyncSession):
#     bom_id = 1  # Adjust this based on your test data
#     result = await plan_dao.Read_Bomid(bom_id, session)

#     # Assert
#     assert result is not None


# @pytest.mark.asyncio
# async def test_plan_read_ganttid(session: AsyncSession):
#     gantt_id = 1  # Adjust this based on your test data
#     result = await plan_dao.Read_Ganttid(gantt_id, session)

#     # Assert
#     assert result is not None


# @pytest.mark.asyncio
# async def test_plan_read_all_date(session: AsyncSession):
#     date_param = "2023-09-01"  # Adjust this based on your test data
#     result = await plan_dao.ReadAll_Date(date_param, session)

#     # Assert
#     assert len(result) > 0


# @pytest.mark.asyncio
# async def test_plan_read_all_period(session: AsyncSession):
#     start_date = "2023-09-01"
#     end_date = "2023-09-10"
#     result = await plan_dao.ReadAll_Period(start_date, end_date, session)

#     # Assert
#     assert len(result) > 0


# @pytest.mark.asyncio
# async def test_plan_update(session: AsyncSession):
#     existing_plan = await plan_dao.Read(
#         1, session
#     )  # Assuming a plan with ID=1 exists
#     existing_plan.company = "UpdatedCompany"
#     result = await plan_dao.Update(existing_plan, session)

#     # Assert
#     assert result.company == "UpdatedCompany"


# @pytest.mark.asyncio
# async def test_plan_delete(session: AsyncSession):
#     plan_to_delete = await plan_dao.Create(
#         Plan(...), session
#     )  # Create a new plan for deletion
#     result = await plan_dao.Delete(plan_to_delete, session)

#     # Assert
#     deleted_plan = await plan_dao.Read(plan_to_delete.id, session)
#     assert deleted_plan is None
