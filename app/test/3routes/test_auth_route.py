import pytest
from unittest.mock import patch, AsyncMock
from httpx import AsyncClient


@pytest.mark.asyncio
@patch("app.services.user_service.input_user", new_callable=AsyncMock)
async def test_signup(mock: AsyncMock, client: AsyncClient):
    mock.return_value = {
        "user_id": "아이디",
        "name": "이름",
        "role": "Worker",
        "id": 1,
        "email": "email@email.com",
    }
    response = await client.post(
        "/auth/signup",
        json={
            "user_id": "Worker1",
            "pass_word": "Worker1",
            "name": "워커1",
            "email": "Worker1@worker.com",
        },
    )
    assert response.status_code == 201
    assert mock.call_count == 1


@pytest.mark.asyncio
@patch("app.services.user_service.login_user", new_callable=AsyncMock)
async def test_login(mock: AsyncMock, client: AsyncClient):
    mock.return_value = {
        "user_id": "아이디",
        "name": "이름",
        "role": "Worker",
        "access_token": "토큰",
        "token_type": "타입",
        "expires_in": 1,
    }
    response = await client.post(
        "/auth/login",
        data={"username": "Master", "password": "Master"},
    )
    assert response.status_code == 201
    assert mock.call_count == 1
