import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_user_post(client: AsyncClient, master_token_headers: dict):
    response = await client.post(
        "/user",
        headers=master_token_headers,
        json={
            "user_id": "Worker1",
            "pass_word": "Worker1",
            "name": "워커1",
            "email": "Worker1@worker.com",
            "role": "Worker",
        },
    )
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_user_get_all(client: AsyncClient, master_token_headers: dict):
    response = await client.get(
        "/user",
        headers=master_token_headers,
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_user_get_all_names(
    client: AsyncClient, master_token_headers: dict
):
    response = await client.get(
        "/user/name?name=",
        headers=master_token_headers,
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_user_put(client: AsyncClient, master_token_headers: dict):
    response = await client.put(
        "/user",
        headers=master_token_headers,
        json={
            "id": 5,
            "user_id": "wolf",
            "pass_word": "wolf",
            "name": "이리",
            "email": "Wolf@wolf.com",
            "role": "Worker",
        },
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_user_delete(client: AsyncClient, master_token_headers: dict):
    response = await client.delete(
        "/user/5",
        headers=master_token_headers,
    )
    assert response.status_code == 200
