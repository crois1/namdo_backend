from datetime import datetime
from fastapi import HTTPException, status, Request, Response, Depends
from typing import Annotated
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import postgresql
from app.models.user import User
from app.dao import user_dao
from app.libs.tokenUtil import verifyToken, setToken


# check Master privileges
async def check_Master(
    request: Request,
):
    # 1. Read From Current User Data & Validate Role
    if not hasattr(request.state, "user") or request.state.user.role not in [
        "Master",
    ]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient privileges(Master)",
        )

    # 2. Return Current User Data
    return request.state.user


# check Admin privileges
async def check_Admin(
    request: Request,
):
    # 1. Read From Current User Data & Validate Role
    if not hasattr(request.state, "user") or request.state.user.role not in [
        "Master",
        "Admin",
    ]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient privileges(Administrator)",
        )

    # 2. Return Current User Data
    return request.state.user


# check Current User
async def check_User(
    token: Annotated[dict[str, str | int], Depends(verifyToken)],
    request: Request,
    response: Response,
    session: AsyncSession = Depends(postgresql.connect),
):
    # 1. if User Data is already in request state, return it
    if hasattr(request.state, "user"):
        return request.state.user

    # 2. Check Request Header Token
    params = User(user_id=token.get("sub"))
    # device_uuid = token.get("uuid")
    # expires_in = datetime.fromtimestamp(token.get("exp"))

    # 3. Get Token User Data
    result = await user_dao.Read_UserID(params.user_id, session)

    # 4. Set Token & Response Header
    # token = await setToken(result, device_uuid, session)
    token = await setToken(result)

    response.headers["Access-Control-Allow-Headers"] = "Authorization"
    response.headers["authorization"] = (
        token["token_type"] + " " + token["access_token"]
    )

    request.state.user = result
    # request.state.uuid = device_uuid
    # request.state.exp = expires_in
    # request.state.session = session

    # 5. Return Current User Data
    return result
