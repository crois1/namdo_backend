from fastapi import HTTPException, status
from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash_password(password):
    return pwd_context.hash(password)


def verify_password(input, exist) -> None:
    if not pwd_context.verify(input, exist):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Password Matched Failed",
        )
    return True
