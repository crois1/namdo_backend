from datetime import timedelta, datetime
from jose import jwt
from typing import Annotated

from fastapi import Request, HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.ext.asyncio import AsyncSession

from app.config import settings
from app.db import postgresql
from app.models.auth import Auth
from app.dao import auth_dao

# from fastapi.responses import JSONResponse

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/login")


# make token at login
def makeToken(payload):
    # 1. Make Token
    try:
        token = jwt.encode(
            payload,
            settings.SECRET_KEY,
            algorithm=settings.ALGORITHM,
        )
    except Exception as e:
        raise e

    # 2. Return JWT Token Encoded
    return token


# verify token at calls
async def verifyToken(
    authorization: Annotated[str, Depends(oauth2_scheme)],
    # session: AsyncSession = Depends(postgresql.connect),
):
    # 1. Verify Token
    try:
        token = jwt.decode(
            authorization,
            settings.SECRET_KEY,
            algorithms=[settings.ALGORITHM],
        )
    except jwt.ExpiredSignatureError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token has expired",
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"Invalid token: {str(e)}",
            headers={"WWW-Authenticate": settings.TOKEN_TYPE},
        )

    # 2. Check Blacklist
    # result = await auth_dao.Read(token.get("uuid"), authorization, session)
    # if result:
    #     raise HTTPException(
    #         status_code=status.HTTP_401_UNAUTHORIZED,
    #         detail="Token Black-out",
    #     )

    # 2. Response
    return token


# set token data for header & OAuth2.0 response
# async def setToken(payload, device_uuid, session):
async def setToken(payload):
    # 1. Set Token Datas
    try:
        payload = {
            # "uuid": device_uuid,
            "sub": payload.user_id,
            "name": payload.name,
            "role": payload.role,
            "exp": datetime.utcnow()
            + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES),
        }

        access_token = makeToken(payload)
        token_type = settings.TOKEN_TYPE
    except Exception as e:
        raise e

    # 2. Register Token into Session
    # blacklist = Auth(
    #     uid=payload.get("sub"),
    #     did=device_uuid,
    #     access_token=access_token,
    #     expires_in=datetime.fromtimestamp(payload.get("exp")),
    # )
    # await auth_dao.Update_Expire_OldDeviceToken(blacklist, session)
    # await auth_dao.Create(blacklist, session)

    # 2. Return at Success
    return {
        "access_token": access_token,
        "token_type": token_type,
        "expires_in": payload.get("exp"),
        "user_id": payload.get("sub"),
        "name": payload.get("name"),
        "role": payload.get("role"),
    }


# set auth header
async def middlewareToken(request: Request, call_next):
    # 1. Skip For Certain URL
    if request.method in ["OPTIONS"]:
        response = await call_next(request)
        return response

    if request.url.path.startswith("/auth"):
        response = await call_next(request)
        return response

    if request.url.path.startswith("/docs") or request.url.path.startswith(
        "/openapi.json"
    ):
        response = await call_next(request)
        return response

    # 2. Verify Header, Set Response Header
    try:
        response = await call_next(request)

        req_auth_header = request.headers.get("authorization")
        res_auth_header = response.headers.get("authorization")
        if req_auth_header is None or res_auth_header is None:
            return response

        print("expired")
        token = await oauth2_scheme(request)
        blacklist = Auth(
            did=request.state.uuid,
            access_token=token,
        )
        await auth_dao.Update_Expire_UsedToken(
            blacklist, request.state.session
        )

    except Exception as e:
        raise e

    # 3. Continue At Success
    return response
